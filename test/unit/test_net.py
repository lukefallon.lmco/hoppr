"""
Test module for net class
"""

# pylint: disable=redefined-outer-name
# pylint: disable=unused-argument

from __future__ import annotations

import json

from pathlib import Path
from typing import Callable

import pytest

from pytest import MonkeyPatch

import hoppr.net
import hoppr.plugin_utils
import hoppr.utils

from hoppr.exceptions import HopprLoadDataError
from hoppr.models.credentials import CredentialRequiredService
from hoppr.net import Credentials, HashAlgorithm, requests  # type: ignore[attr-defined]

test_input_data = {
    "alpha": [1, 2, 3],
    "beta": ["dog", "cat"],
    "gamma": {"x": 42, "y": "why not", "z": ["mixed", 7, "array"]},
}


@pytest.mark.parametrize(
    argnames=["algorithm", "expected_hash"],
    argvalues=[
        pytest.param(
            HashAlgorithm.BLAKE2B,
            "443e7aa109dd46978a4ab865dbda0cbe3cfcead7e890f8a3347079b908e3ec6f"
            "bc2092a4969f7a53ee15da686d9ddb3715a7302a1a66f64b13e4278d271e04b8",
            id="BLAKE2B",
        ),
        pytest.param(
            HashAlgorithm.BLAKE2S,
            "89769579bb89df6e875b81e0f10320946796ea0eaa150c6c634c8f5f409bf55b",
            id="BLAKE2S",
        ),
        pytest.param(
            HashAlgorithm.MD5,
            "d8d4c2d1c8e4b04d96bca23175d071c5",
            id="MD5",
        ),
        pytest.param(
            HashAlgorithm.SHA1,
            "381e08efd0d8182d2a559321b2b60234010f74bc",
            id="SHA1",
        ),
        pytest.param(
            HashAlgorithm.SHA224,
            "ba3b6dad72628e410bba30558503d850253b3efa250690bf3e91cf1e",
            id="SHA224",
        ),
        pytest.param(
            HashAlgorithm.SHA256,
            "bf79be0c21a100565100d16b31deee78ce5391f66c0774405d484ce38b6076e0",
            id="SHA256",
        ),
        pytest.param(
            HashAlgorithm.SHA384,
            "1a380fe6b1ce001c5d2ae4e57c23c0249ca6d2ee875ee7ed611976e4423994b059e5d29c26f8fa73d0c60873c0abc637",
            id="SHA384",
        ),
        pytest.param(
            HashAlgorithm.SHA512,
            "ea70fa7712fb3e378c18ee1026bfe79bb9b099894b3dff868c5015051ec8363e"
            "c29f66f39f392962e1fc619bebcf89d8c6e334c387f6a6701e8d475794cb823b",
            id="SHA512",
        ),
    ],
)
def test_get_file_hash(algorithm: HashAlgorithm, expected_hash: str, tmp_path: Path):
    """
    Test hoppr.net.get_file_hash function
    """
    artifact = tmp_path / "test-artifact.txt"
    artifact.write_bytes(b"0" * 1048576)

    assert hoppr.net.get_file_hash(artifact, algorithm) == expected_hash


@pytest.mark.parametrize(
    argnames=["enum_member", "expected_result"],
    argvalues=[
        pytest.param(HashAlgorithm.BLAKE2B, "blake2b", id="BLAKE2B"),
        pytest.param(HashAlgorithm.BLAKE2S, "blake2s", id="BLAKE2S"),
        pytest.param(HashAlgorithm.MD5, "md5", id="MD5"),
        pytest.param(HashAlgorithm.SHA1, "sha1", id="SHA1"),
        pytest.param(HashAlgorithm.SHA224, "sha224", id="SHA224"),
        pytest.param(HashAlgorithm.SHA256, "sha256", id="SHA256"),
        pytest.param(HashAlgorithm.SHA384, "sha384", id="SHA384"),
        pytest.param(HashAlgorithm.SHA512, "sha512", id="SHA512"),
    ],
)
def test_hash_algorithm__str__(enum_member: HashAlgorithm, expected_result: str):
    """
    Test HashAlgorithm.__str__ method
    """
    assert str(enum_member) == expected_result


def test_load_url(
    find_credentials_fixture: CredentialRequiredService,
    response_fixture: Callable[[str], requests.Response],
    monkeypatch: MonkeyPatch,
):
    """
    Test hoppr.net.load_url function
    """
    monkeypatch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
    monkeypatch.setattr(target=requests, name="get", value=response_fixture)

    content = hoppr.net.load_url(url="url_goes_here")
    assert content == test_input_data


@pytest.mark.parametrize(argnames="response_fixture", argvalues=[{"content": test_input_data}], indirect=True)
def test_load_url_unsupported_input_type(
    monkeypatch: MonkeyPatch, response_fixture: Callable[[str], requests.Response]
):
    """
    Test hoppr.net.load_url function failure due to unsupported input
    """
    monkeypatch.setattr(target=requests, name="get", value=response_fixture)

    with pytest.raises(expected_exception=HopprLoadDataError):
        hoppr.net.load_url(url="url_goes_here")


@pytest.mark.parametrize(
    argnames="response_fixture", argvalues=[{"content": json.dumps(test_input_data).encode("utf-8")}], indirect=True
)
def test_load_url_bytes(monkeypatch: MonkeyPatch, response_fixture: Callable[[str], requests.Response]):
    """
    Test hoppr.net.load_url function with byte data
    """
    monkeypatch.setattr(target=requests, name="get", value=response_fixture)

    content = hoppr.net.load_url(url="url_goes_here")
    assert content == test_input_data


@pytest.mark.parametrize(argnames="response_fixture", argvalues=[{"content": "BAD DATA"}], indirect=True)
def test_load_url_bad_data(monkeypatch: MonkeyPatch, response_fixture: Callable[[str], requests.Response]):
    """
    Test hoppr.net.load_url function raises exception given bad data
    """
    monkeypatch.setattr(target=requests, name="get", value=response_fixture)

    with pytest.raises(expected_exception=HopprLoadDataError):
        hoppr.net.load_url(url="url_goes_here")


@pytest.mark.parametrize(argnames="response_fixture", argvalues=[{"content": b"0" * 1024}], indirect=True)
def test_download_file(
    monkeypatch: MonkeyPatch,
    cred_object_fixture: CredentialRequiredService,
    find_credentials_fixture: Callable[[str], CredentialRequiredService],
    response_fixture: Callable[[str], requests.Response],
    tmp_path: Path,
):
    """
    Test hoppr.net.download_file function
    """
    monkeypatch.setattr(target=requests, name="get", value=response_fixture)
    monkeypatch.setattr(target=Credentials, name="find", value=find_credentials_fixture)

    download_path = tmp_path / "test_net::test_download_file"
    response = hoppr.net.download_file(url=cred_object_fixture.url, dest=str(download_path))

    assert response.status_code == 200
    assert download_path.is_file()
    assert download_path.read_bytes() == b"0" * 1024


@pytest.mark.parametrize(argnames="response_fixture", argvalues=[{"content": b"0" * 1024}], indirect=True)
def test_download_file_without_credentials(
    monkeypatch: MonkeyPatch, response_fixture: Callable[[str], requests.Response], tmp_path: Path
):
    """
    Test hoppr.net.download_file function without credentials
    """
    monkeypatch.setattr(target=requests, name="get", value=response_fixture)

    download_path = tmp_path / "test_net::test_download_file"
    response = hoppr.net.download_file(url="http://test.hoppr.com", dest=str(download_path))

    assert response.status_code == 200
    assert download_path.is_file()
    assert download_path.read_bytes() == b"0" * 1024
