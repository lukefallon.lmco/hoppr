"""
Test module for manifest pydantic models
"""
from __future__ import annotations

from datetime import datetime
from pathlib import Path
from typing import TYPE_CHECKING, Callable, Type, TypeVar

import pytest

from pytest import FixtureRequest, MonkeyPatch

import hoppr
import hoppr.net
import hoppr.oci_artifacts
import hoppr.utils

from hoppr.exceptions import HopprLoadDataError
from hoppr.models.manifest import Manifest, ManifestFile, Repository
from hoppr.models.sbom import Component, Property, Sbom
from hoppr.models.types import LocalFile, OciFile, RepositoryUrl, UrlFile

if TYPE_CHECKING:
    from pydantic.typing import DictStrAny

    from hoppr.models.sbom import SbomRef

    Sboms = list[SbomRef]

# pylint: disable=protected-access, unused-argument

ExpectedException = TypeVar("ExpectedException", bound=BaseException)


@pytest.fixture(autouse=True)
def clear_loaded_data():
    """
    Fixture to automatically reset loaded SBOMs and includes before each test
    """
    Manifest.loaded_manifests.clear()
    Sbom.loaded_sboms.clear()
    Component.unique_id_map.clear()


@pytest.fixture(name="includes")
def includes_fixture(request: FixtureRequest) -> list[DictStrAny]:
    """
    Fixture to return `includes` object
    """
    if hasattr(request, "param"):
        return request.param

    return [
        {"local": "test/resources/manifest/manifest.yml"},
        {"local": "test/resources/manifest/unit/manifest.yml"},
        {"url": "https://test.hoppr.com/helm/manifest.yml"},
        {"url": "https://test.hoppr.com/pypi/manifest.yml"},
    ]


@pytest.fixture(name="manifest_dict")
def manifest_dict_fixture(includes: list[DictStrAny], sboms: list[DictStrAny]) -> DictStrAny:
    """
    Fixture to return dict representation of a ManifestFile object
    """
    return {
        "kind": "Manifest",
        "metadata": {
            "name": "Unit Test Manifest",
            "timestamp": datetime(year=1970, month=1, day=1),
            "version": "0.1.0",
            "description": "Manifest for unit tests",
        },
        "schemaVersion": "v1",
        "repositories": {
            "deb": [
                {"url": "https://apt.repo.alpha.com"},
                {"url": "https://apt.repo.beta.com"},
            ],
            "generic": [{"url": "file://"}],
            "pypi": [{"url": "https://pypi.hoppr.com/repositories/pypi"}],
            "rpm": [
                {"url": "https://rpm.hoppr.com/dl/8/AppStream/x86_64/os"},
                {"url": "https://rpm.hoppr.com/dl/8/BaseOS/x86_64/os"},
                {"url": "https://rpm.hoppr.com/dl/8/PowerTools/x86_64/os"},
            ],
        },
        "includes": includes or [],
        "sboms": sboms or [],
    }


@pytest.fixture(name="manifest")
def manifest_fixture(resources_dir: Path) -> Manifest:
    """
    Fixture to return Manifest object
    """
    return Manifest.load(resources_dir / "manifest" / "unit" / "manifest.yml")


@pytest.fixture(name="sboms")
def sboms_fixture(request: FixtureRequest) -> list[DictStrAny]:
    """
    Fixture to return `sboms` object
    """
    if hasattr(request, "param"):
        return request.param

    return [
        {"local": "../../bom/unit_bom1_mini.json"},
        {"local": "../../bom/unit_bom2_mini.json"},
        {"url": "http://test.hoppr.com/sbom_1.json"},
        {"url": "http://test.hoppr.com/sbom_2.json"},
    ]


def test_manifest_file_parse_file(resources_dir: Path):
    """
    Test ManifestFile.parse_file method
    """
    manifest_path = resources_dir / "manifest" / "unit" / "manifest.yml"
    ManifestFile.parse_file(manifest_path)


def test_manifest_file_parse_file_fail(monkeypatch: MonkeyPatch, resources_dir: Path):
    """
    Test ManifestFile.parse_file method fail to load file
    """
    manifest_path = resources_dir / "manifest" / "unit" / "manifest.yml"

    monkeypatch.setattr(target=hoppr.utils, name="load_file", value=lambda file: None)

    with pytest.raises(expected_exception=TypeError) as pytest_exception:
        ManifestFile.parse_file(manifest_path)

    assert pytest_exception.value.args[0] == "Local file content was not loaded as dictionary"


def test_manifest_file_parse_obj(manifest: Manifest):
    """
    Test ManifestFile.parse_obj method
    """
    manifest_dict = manifest.dict(by_alias=True)

    manifest_dict["includes"] = [
        {"local": "test/resources/manifest/manifest.yml"},
        {"local": "test/resources/manifest/unit/manifest.yml"},
    ]

    manifest_dict["sboms"] = [
        {"local": "../../bom/unit_bom1_mini.json"},
        {"local": "../../bom/unit_bom2_mini.json"},
    ]

    manifest_obj = ManifestFile.parse_obj(manifest_dict)

    # Test that local refs were removed
    assert len(manifest_obj.includes) == 0
    assert len(manifest_obj.sboms) == 0


def test_manifest__add_component(manifest: Manifest):
    """
    Test Manifest._add_component method
    """
    component = Component(
        name="something/else",
        version="1.2.3",
        purl="pkg:good1/something/else@1.2.3",
        type="file",  # type: ignore[arg-type]
        properties=[],
    )

    # Test adding component that for some reason isn't in the `unique_id_map`
    Component.unique_id_map.clear()
    manifest._add_component(component)
    assert Component.find(component.bom_ref)

    manifest.consolidated_sbom = Sbom()  # pyright: ignore

    # Test adding component without property
    manifest._add_component(component)
    assert len(component.properties or []) == 0

    # Test adding same component with property merges the property with existing component
    new_component = component.copy(deep=True)
    new_component.properties = [Property(name="hoppr:test:name", value="hoppr:test:value")]

    manifest._add_component(new_component)
    assert len(component.properties or []) == 1

    # Test idempotency
    manifest._add_component(new_component)
    assert len(component.properties or []) == 1


@pytest.mark.parametrize(argnames=["includes", "sboms"], argvalues=[([], [])], indirect=["includes", "sboms"])
def test_manifest_find(manifest_dict: DictStrAny):
    """
    Test Manifest.find method
    """
    manifest_path = Path("test") / "resources" / "manifest" / "unit" / "manifest.yml"
    manifest_url = "https://test.hoppr.com/helm/manifest.yml"

    manifest = Manifest.load(manifest_dict)

    Manifest.loaded_manifests[LocalFile(local=manifest_path)] = manifest
    Manifest.loaded_manifests[UrlFile(url=manifest_url)] = manifest

    manifest_obj = Manifest.find("local", manifest_path)
    assert manifest_obj == manifest

    manifest_obj = Manifest.find("url", manifest_url)
    assert manifest_obj == manifest

    # Test that None is returned for invalid include type
    assert Manifest.find(ref_type="invalid", location="nonexistent/path") is None  # type: ignore[arg-type]


def test_manifest_includes_validation_error(manifest: Manifest, monkeypatch: MonkeyPatch):
    """
    Test pydantic validator for Manifest.includes attribute raises TypeError
    """
    with monkeypatch.context() as patch:
        patch.setattr(target=hoppr.utils, name="load_file", value=lambda file: None)
        patch.setattr(target=hoppr.net, name="load_url", value=lambda file: None)
        patch.setattr(target=manifest, name="sboms", value=[])
        patch.setattr(target=Manifest, name="loaded_manifests", value={})

        with pytest.raises(expected_exception=TypeError) as pytest_exception:
            Manifest._load_include(UrlFile(url="https://test.hoppr.com/manifest.yml"))

        assert pytest_exception.value.args[0] == "URL manifest include was not loaded as dictionary"


def test_manifest_includes_validator(manifest: Manifest, monkeypatch: MonkeyPatch):
    """
    Test pydantic validator for Manifest.includes attribute
    """
    monkeypatch.setattr(target=hoppr.net, name="load_url", value=lambda url: manifest.dict(by_alias=True))
    monkeypatch.setattr(target=hoppr.utils, name="load_file", value=lambda path: manifest.dict(by_alias=True))

    Manifest.populate_loaded_manifests(
        includes=[
            LocalFile(local=Path("manifest.yml")),
            UrlFile(url="https://test.hoppr.com/manifest_1.yml"),
        ],
        values={"repositories": manifest.repositories},
    )


@pytest.mark.parametrize(argnames=["includes", "sboms"], argvalues=[([], [])], indirect=True)
def test_manifest_load_dict(manifest_dict: DictStrAny):
    """
    Test Manifest.load method with dict input
    """
    Manifest.load(manifest_dict)


def test_manifest_load_file(resources_dir: Path):
    """
    Test Manifest.load method with Path input
    """
    manifest_path = resources_dir / "manifest" / "unit" / "manifest.yml"
    manifest = Manifest.load(manifest_path)

    assert manifest.repositories["deb"] == [
        Repository.parse_obj({"url": "https://apt.repo.alpha.com"}),
        Repository.parse_obj({"url": "https://apt.repo.beta.com"}),
    ]
    assert manifest.repositories["generic"] == [Repository.parse_obj({"url": "file://"})]
    assert manifest.repositories["pypi"] == [Repository.parse_obj({"url": "https://pypi.hoppr.com/repositories/pypi"})]
    assert manifest.repositories["rpm"] == [
        Repository.parse_obj({"url": "https://rpm.hoppr.com/dl/8/AppStream/x86_64/os"}),
        Repository.parse_obj({"url": "https://rpm.hoppr.com/dl/8/BaseOS/x86_64/os"}),
        Repository.parse_obj({"url": "https://rpm.hoppr.com/dl/8/PowerTools/x86_64/os"}),
    ]

    include = Manifest.find(ref_type="local", location=manifest_path.with_name("child-a.yml"))
    assert include is not None

    # Test Repositories.__setitem__ method by adding repositories expected in included manifest
    expected_include_repos = manifest.repositories.copy(deep=True)
    expected_include_repos["deb"].append(Repository.parse_obj({"url": "https://apt.repo.gamma.com"}))
    expected_include_repos["pypi"].append(Repository.parse_obj({"url": "https://another.pypy.repo.com/"}))
    expected_include_repos["pypi"].append(Repository.parse_obj({"url": "https://pypi.org"}))

    assert include.repositories == expected_include_repos

    # Test Repositories.__iter__ method
    for purl_type_repo_list in manifest.repositories:
        assert isinstance(purl_type_repo_list, tuple)

    # Test consolidated_sbom expected values
    assert len(Sbom.loaded_sboms) == 3

    num_components = sum(len(sbom.components) for sbom in Sbom.loaded_sboms.values())

    assert num_components == 8

    assert manifest.consolidated_sbom
    assert len(manifest.consolidated_sbom.components) == 6  # (8 total) - (2 duplicates)

    assert manifest.consolidated_sbom.components[0].externalReferences
    assert manifest.consolidated_sbom.components[1].externalReferences
    assert manifest.consolidated_sbom.components[2].externalReferences
    assert manifest.consolidated_sbom.components[3].externalReferences
    assert manifest.consolidated_sbom.components[4].externalReferences

    assert len(manifest.consolidated_sbom.components[0].externalReferences) == 2
    assert len(manifest.consolidated_sbom.components[1].externalReferences) == 1
    assert len(manifest.consolidated_sbom.components[2].externalReferences) == 2
    assert len(manifest.consolidated_sbom.components[3].externalReferences) == 1
    assert len(manifest.consolidated_sbom.components[4].externalReferences) == 1


@pytest.mark.parametrize(
    argnames=["load_url_fixture", "expected_exception", "expected_message"],
    argvalues=[
        ({}, None, None),
        ([], TypeError, "URL manifest include was not loaded as dictionary"),
        ("HopprLoadDataError", HopprLoadDataError, ""),
        ("HTTPError", HopprLoadDataError, ""),
    ],
    indirect=["load_url_fixture"],
)
def test_manifest_load_url(
    manifest: Manifest,
    load_url_fixture: Callable[..., object],
    expected_exception: Type[ExpectedException] | None,
    expected_message: str,
    monkeypatch: MonkeyPatch,
):
    """
    Test Manifest.load method with URL input
    """
    manifest_url = "https://test.hoppr.com/manifest.yml"

    with monkeypatch.context() as patch:
        patch.setattr(target=ManifestFile, name="parse_obj", value=lambda obj: manifest)
        patch.setattr(target=hoppr.net, name="load_url", value=load_url_fixture)

        if expected_exception is not None:
            with pytest.raises(expected_exception, match=expected_message) as ex:
                Manifest.load(manifest_url)

            assert ex.type == expected_exception
        else:
            Manifest.load(manifest_url)


@pytest.mark.parametrize(
    argnames="sbom_refs",
    argvalues=[
        [OciFile(oci="oci://test.hoppr.com/oci-sbom.json")],
        [UrlFile(url="https://test.hoppr.com/url-sbom.json")],
    ],
)
def test_manifest_sboms_validation_error(sbom_refs: Sboms, monkeypatch: MonkeyPatch):
    """
    Test pydantic validator for Manifest.sboms attribute raises TypeError
    """
    with monkeypatch.context() as patch:
        patch.setattr(target=hoppr.net, name="load_url", value=lambda url: None)
        patch.setattr(target=hoppr.oci_artifacts, name="pull_artifact", value=lambda artifact: None)
        patch.setattr(target=hoppr.utils, name="load_file", value=lambda path: None)

        with pytest.raises(expected_exception=TypeError):
            Manifest.populate_loaded_sboms(sbom_refs)


@pytest.mark.parametrize(
    argnames="sbom_refs",
    argvalues=[
        [LocalFile(local=Path("../hoppr/test/local-sbom.json"))],
        [OciFile(oci="oci://test.hoppr.com/oci-sbom.json")],
        [UrlFile(url="https://test.hoppr.com/url-sbom.json")],
        [None],
    ],
)
def test_manifest_sboms_validator(sbom_refs: Sboms, manifest: Manifest, monkeypatch: MonkeyPatch, resources_dir: Path):
    """
    Test pydantic validator for Manifest.sboms attribute
    """
    sbom = Sbom.parse_file(resources_dir / "bom" / "unit_bom1_mini.json")
    sbom_dict = sbom.dict(by_alias=True)

    with monkeypatch.context() as patch:
        patch.setattr(target=hoppr.net, name="load_url", value=lambda url: sbom_dict)
        patch.setattr(target=hoppr.oci_artifacts, name="pull_artifact", value=lambda path: sbom_dict)
        patch.setattr(target=Sbom, name="parse_file", value=lambda path: sbom)
        patch.setattr(target=Sbom, name="loaded_sboms", value={})

        Manifest.populate_loaded_sboms(sbom_refs)


def test_repository_url_validator():
    """
    Test pydantic validator for Repository.url attribute
    """
    assert Repository.validate_url(url="file:") == RepositoryUrl(url="file://")
    assert Repository.validate_url(url="docker.io") == RepositoryUrl(url="docker.io")
