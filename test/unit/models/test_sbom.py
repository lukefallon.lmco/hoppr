"""
Test module for CycloneDX pydantic models
"""
from __future__ import annotations

from datetime import datetime
from pathlib import Path
from typing import TYPE_CHECKING, Callable, TypeVar

import pytest

from pytest import FixtureRequest, MonkeyPatch

import hoppr.net

from hoppr.exceptions import HopprLoadDataError
from hoppr.models.base import CycloneDXBaseModel
from hoppr.models.sbom import Component, ComponentType, Hash, Sbom, Tools
from hoppr.models.types import LocalFile, OciFile, UrlFile


if TYPE_CHECKING:
    from pydantic.typing import DictStrAny


ExpectedException = TypeVar("ExpectedException", bound=BaseException)


@pytest.fixture(autouse=True)
def clear_loaded_data():
    """
    Fixture to automatically reset loaded SBOMs before each test
    """
    CycloneDXBaseModel.deep_merge = False
    CycloneDXBaseModel.flatten = False

    Component.unique_id_map.clear()
    Sbom.loaded_sboms.clear()
    Sbom.unique_id_map.clear()


@pytest.fixture(name="components_dict")
def components_dict_fixture(external_refs_dict: list[DictStrAny], request: FixtureRequest) -> list[DictStrAny]:
    """
    Fixture to return list of `Component` dicts
    """
    if hasattr(request, "param"):
        return request.param

    return [
        {
            "name": "test-component-1",
            "version": "0.1.2",
            "type": "library",
            "components": [
                {
                    "name": "test-component-2",
                    "version": "3.4.5",
                    "type": "library",
                }
            ],
            "externalReferences": external_refs_dict or [],
        }
    ]


@pytest.fixture(name="external_refs_dict")
def external_refs_dict_fixture(request: FixtureRequest) -> list[DictStrAny]:
    """
    Fixture to return list of `Component` dicts
    """
    if hasattr(request, "param"):
        return request.param

    return [
        {"type": "bom", "url": "https://test.hoppr.com/sbom.json"},
        {"type": "distribution", "url": "https://test.hoppr.com/dist.tgz"},
    ]


@pytest.fixture(name="sbom_dict")
def sbom_dict_fixture(
    components_dict: list[DictStrAny],
    external_refs_dict: list[DictStrAny],
    vulnerabilities_dict: list[DictStrAny],
) -> DictStrAny:
    """
    Fixture to return `Sbom` dict
    """
    return {
        "bomFormat": "CycloneDX",
        "specVersion": "1.4",
        "version": 1,
        "components": components_dict or [],
        "externalReferences": external_refs_dict or [],
        "vulnerabilities": vulnerabilities_dict or [],
    }


@pytest.fixture(name="sbom")
def sbom_fixture(resources_dir: Path, request: FixtureRequest) -> Sbom:
    """
    Fixture to return parsed `Sbom` object
    """
    sbom_file = getattr(request, "param", resources_dir / "bom" / "unit_bom1_mini.json")
    return Sbom.load(sbom_file)


@pytest.fixture(name="vulnerabilities_dict")
def vulnerabilities_dict_fixture(request: FixtureRequest) -> list[DictStrAny]:
    """
    Fixture to return list of `Vulnerability` dicts
    """
    if hasattr(request, "param"):
        return request.param

    return [
        {
            "id": "CVE-1234-5678",
            "source": {"name": "test-source", "url": "https://test-source.org"},
            "ratings": [{"source": {"name": "test-source"}, "severity": "low"}],
            "cwes": [9999],
            "description": "Vulnerability for unit tests",
            "advisories": [{"url": "https://test-source.org/CVERecord?id=CVE-1234-5678"}],
            "published": "2019-12-03T15:15:00+00:00",
            "updated": "2023-02-13T00:28:00+00:00",
            "affects": [
                {
                    "ref": "pkg:deb/debian/test-component-1@0.1.2",
                    "versions": [{"version": "0.1.2", "status": "affected"}],
                },
                {
                    "ref": "pkg:deb/debian/test-component-1@3.4.5",
                    "versions": [{"version": "vers:deb/>=3.0.0|<4.0.0", "status": "affected"}],
                },
            ],
        }
    ]


def test__validate_components(sbom_dict: DictStrAny, monkeypatch: MonkeyPatch):
    """
    Test _validate_components function
    """
    monkeypatch.setattr(target=CycloneDXBaseModel, name="flatten", value=True)

    sbom = Sbom.parse_obj(sbom_dict)

    assert len(sbom.components) == 2
    assert len(sbom.components[0].components) == 0
    assert sbom.components[0].bom_ref == "test-component-1@0.1.2"

    assert len(sbom.components[1].components) == 0
    assert sbom.components[1].bom_ref == "test-component-2@3.4.5"


@pytest.mark.parametrize(
    argnames="load_url_fixture",
    argvalues=[
        {
            "bomFormat": "CycloneDX",
            "specVersion": "1.4",
            "version": 1,
            "components": [
                {
                    "name": "test-component-3",
                    "version": "6.7.8",
                    "type": "library",
                }
            ],
            "externalReferences": [],
        }
    ],
    indirect=["load_url_fixture"],
)
def test__validate_external_refs(
    sbom_dict: DictStrAny,
    load_url_fixture: Callable[[str], object],
    monkeypatch: MonkeyPatch,
):
    """
    Test _validate_external_refs function
    """
    monkeypatch.setattr(target=CycloneDXBaseModel, name="deep_merge", value=True)
    monkeypatch.setattr(target=hoppr.net, name="load_url", value=load_url_fixture)

    sbom = Sbom.load(sbom_dict)

    # Verify component was loaded from `externalReferences` with scope set to "excluded"
    merged_comp = Component.find("test-component-3@6.7.8")
    assert merged_comp
    assert str(merged_comp.scope) == "excluded"

    # Verify all `externalReferences` of type "bom" were removed
    external_refs = [
        *(sbom.externalReferences or []),
        *(sbom.components[0].externalReferences or []),
        *(sbom.components[0].components[0].externalReferences or []),
    ]

    bom_refs = [ref for ref in external_refs if ref.type == "bom"]
    assert not bom_refs


def test_base_merge():
    """
    Test CycloneDXBaseModel.merge method
    """
    base_sbom = Sbom.parse_obj(
        {
            "metadata": {"timestamp": datetime.now().isoformat()},
            "components": [
                {
                    "name": "test-component-1",
                    "version": "0.1.2",
                    "type": "library",
                    "purl": "pkg:generic/test-component-1@0.1.2",
                },
            ],
            "externalReferences": [
                {
                    "type": "bom",
                    "url": "https://test.hoppr.com/sbom.json",
                }
            ],
        }
    )

    other_sbom = Sbom.parse_obj(
        {
            "metadata": {"timestamp": datetime.now().isoformat()},
            "components": [
                {
                    "name": "test-component-3",
                    "version": "6.7.8",
                    "type": "library",
                    "purl": "pkg:generic/test-component-3@6.7.8",
                },
            ],
            "externalReferences": [
                {
                    "type": "bom",
                    "url": "https://test.hoppr.com/sbom.json",
                    "comment": "additional field to merge",
                }
            ],
        }
    )

    base_sbom.merge(other_sbom)

    # Test idempotence by copying merged SBOM and re-performing the merge
    sbom_copy = base_sbom.copy(deep=True)
    sbom_copy.merge(base_sbom)
    sbom_copy.merge(other_sbom)
    assert sbom_copy == base_sbom


def test_base_merge_exception(sbom_dict: DictStrAny, components_dict: list[DictStrAny]):
    """
    Test CycloneDXBaseModel.merge method raises TypeError
    """
    sbom = Sbom.parse_obj(sbom_dict)
    component = Component.parse_obj(components_dict[0])

    with pytest.raises(expected_exception=TypeError, match="Type 'Component' cannot be merged into 'Sbom'"):
        sbom.merge(other=component)


@pytest.mark.parametrize(
    argnames=["component_data", "other_data", "expected_result"],
    argvalues=[
        pytest.param(
            {"name": "test-component-1", "type": "library", "version": "0.1.2"},
            {"name": "test-component-1", "type": "library", "version": "0.1.2"},
            True,
            id="equal",
        ),
        pytest.param(
            {"name": "test-component-1", "type": "library", "version": "0.1.2"},
            {"name": "test-component-1", "type": "library", "version": "0.1.3"},
            False,
            id="not-equal",
        ),
    ],
)
def test_component__eq__(component_data: dict[str, str], other_data: dict[str, str], expected_result: bool):
    """
    Test Component.__eq__ method with no `purl` or `bom-ref` fields
    """
    component = Component.parse_obj(component_data)
    other = Component.parse_obj(other_data)

    assert (component == other) == expected_result


def test_component__eq__not_component_object():
    """
    Test Component.__eq__ method with non-Component object
    """
    component = Component(
        name="test-component-1",
        version="0.1.2",
        type=ComponentType.LIBRARY,
        purl="pkg:generic/test-component-1@0.1.2",
    )

    assert component != "WRONG TYPE"


@pytest.mark.parametrize(
    argnames=["self_hash", "other_hash", "expected_result"],
    argvalues=[
        pytest.param(
            {"alg": "SHA-1", "content": "381e08efd0d8182d2a559321b2b60234010f74bc"},
            {"alg": "SHA-1", "content": "381e08efd0d8182d2a559321b2b60234010f74bc"},
            True,
            id="same-alg-same-content",
        ),
        pytest.param(
            {"alg": "SHA-1", "content": "381e08efd0d8182d2a559321b2b60234010f74bc"},
            {"alg": "SHA-1", "content": "cfd58e1a75412012542791cdbb9f6c4242bc0908"},
            False,
            id="same-alg-different-content",
        ),
        pytest.param(
            {"alg": "SHA-1", "content": "381e08efd0d8182d2a559321b2b60234010f74bc"},
            {"alg": "MD5", "content": "d8d4c2d1c8e4b04d96bca23175d071c5"},
            True,
            id="different-alg",
        ),
    ],
)
def test_component__hash_match(self_hash: dict[str, str], other_hash: dict[str, str], expected_result: bool):
    """
    Test Component._hash_match method
    """
    base_component = Component(
        name="test-component-1",
        version="0.1.2",
        type=ComponentType.LIBRARY,
        purl="pkg:generic/test-component-1@0.1.2",
    )

    other_component = base_component.copy(deep=True)

    base_component.hashes = [Hash.parse_obj(self_hash)]
    other_component.hashes = [Hash.parse_obj(other_hash)]

    assert base_component._hash_match(other_component) == expected_result  # pylint: disable=protected-access


def test_sbom__eq__():
    """
    Test Sbom.__eq__ method
    """
    sbom = Sbom()

    assert sbom != "Sbom"
    assert sbom == sbom.copy(deep=True)


def test_sbom_find_ref(resources_dir: Path):
    """
    Test Sbom.find method
    """
    sbom_path = resources_dir / "bom" / "unit_bom1_mini.json"
    sbom = Sbom.parse_file(sbom_path)

    Sbom.loaded_sboms[LocalFile(local=Path("local-sbom.json"))] = sbom
    Sbom.loaded_sboms[OciFile(oci="oci://test.hoppr.com/oci-sbom.json")] = sbom
    Sbom.loaded_sboms[UrlFile(url="https://test.hoppr.com/url-sbom.json")] = sbom

    assert Sbom.find_ref(ref_type="local", location=Path("local-sbom.json")) == sbom
    assert Sbom.find_ref(ref_type="oci", location="oci://test.hoppr.com/oci-sbom.json") == sbom
    assert Sbom.find_ref(ref_type="url", location="https://test.hoppr.com/url-sbom.json") == sbom
    assert Sbom.find_ref(ref_type="invalid", location=Path("local-sbom.json")) is None  # type: ignore[arg-type]


def test_sbom_load_dict(sbom_dict: dict):
    """
    Test Sbom.load method with dict input
    """
    Sbom.load(sbom_dict)


def test_sbom_load_file(resources_dir: Path):
    """
    Test Sbom.load method with Path input
    """
    sbom = Sbom.load(resources_dir / "bom" / "unit_bom1_mini.json")
    assert isinstance(hash(sbom), int)

    assert sbom.components is not None
    for component in sbom.components:
        hashable = Component.parse_obj(component.dict(by_alias=True))
        assert isinstance(hash(hashable), int)


@pytest.mark.parametrize(
    argnames=["load_url_fixture", "expected_exception", "expected_message"],
    argvalues=[
        ({}, None, None),
        ([], TypeError, "URL SBOM was not loaded as dictionary"),
        ("HopprLoadDataError", HopprLoadDataError, ""),
        ("HTTPError", HopprLoadDataError, ""),
    ],
    indirect=["load_url_fixture"],
)
def test_sbom_load_url(
    sbom_dict: DictStrAny,
    load_url_fixture: Callable[[str], object],
    expected_exception: type[ExpectedException] | None,
    expected_message: str,
    monkeypatch: MonkeyPatch,
):
    """
    Test Sbom.load method with URL input
    """
    sbom_url = "https://test.hoppr.com/sbom.json"

    if expected_exception is not None:
        monkeypatch.setattr(target=hoppr.net, name="load_url", value=load_url_fixture)
        with pytest.raises(expected_exception, match=expected_message) as ex:
            Sbom.load(sbom_url)

            assert ex.type == expected_exception
    else:
        monkeypatch.setattr(target=hoppr.net, name="load_url", value=lambda url: sbom_dict)
        Sbom.load(sbom_url)


@pytest.mark.parametrize(
    argnames="components_dict",
    argvalues=[pytest.param([{"name": "test-component", "type": "application"}], id="missing-version")],
    indirect=True,
)
def test_sbom_validator_missing_component_version(sbom_dict: DictStrAny):
    """
    Test Sbom validation fails given a component with no `purl`, `bom-ref`, or `version` field
    """
    with pytest.raises(
        expected_exception=ValueError,
        match="Either 'bom-ref' or 'purl' must be defined, or 'name' and 'version' must be defined on a component",
    ):
        Sbom.load(sbom_dict)


tools_data = {
    "components": [
        {
            "name": "test-tool",
            "version": "1.2.3",
            "externalReferences": [],
            "hashes": [],
            "scope": "excluded",
            "type": "application",
        }
    ]
}


@pytest.mark.parametrize(
    argnames=["values", "expected_result"],
    argvalues=[
        pytest.param(tools_data, tools_data, id="dict-input"),
        pytest.param([{"name": "test-tool", "version": "1.2.3"}], tools_data, id="list-input"),
        pytest.param(None, None, id="none-input"),
    ],
)
def test_tools_validator(values: DictStrAny | list[DictStrAny] | None, expected_result: DictStrAny | None):
    """
    Test pydantic validator for Tools model
    """
    assert Tools.validate_tools(values) == expected_result
