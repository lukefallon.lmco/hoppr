"""
Test module for CycloneDX pydantic models
"""
from __future__ import annotations

from datetime import datetime, timezone

import hoppr_cyclonedx_models.cyclonedx_1_5 as cdx
import pytest

from pytest import FixtureRequest

from hoppr.models.affect import Affect, AffectVersionRangeRequired, AffectVersionVersionRequired
from hoppr.models.sbom import Vulnerability


@pytest.fixture(name="vulnerability")
def vulnerability_fixture(request: FixtureRequest) -> Vulnerability:
    """
    Fixture to return list of `Vulnerability` dicts
    """
    if hasattr(request, "param"):
        return request.param

    return Vulnerability(
        id="CVE-1234-5678",
        source=cdx.VulnerabilitySource(url="https://test-source.org", name="test-source"),
        ratings=[cdx.Rating(source=cdx.VulnerabilitySource(name="test-source"), severity=cdx.Severity.LOW)],
        cwes=[9999],
        description="Vulnerability for unit tests",
        advisories=[cdx.Advisory(url="https://test-source.org/CVERecord?id=CVE-1234-5678")],
        created=None,
        published=datetime(2019, 12, 3, 15, 15, tzinfo=timezone.utc),
        updated=datetime(2023, 2, 13, 0, 28, tzinfo=timezone.utc),
        affects=[],
    )


def test_merge_affects(vulnerability: Vulnerability):
    """
    Test merging Affect models
    """
    vuln = vulnerability.copy(deep=True)
    other = vulnerability.copy(deep=True)

    vuln.affects = [
        Affect(
            ref="pkg:deb/debian/test-component-1@0.1.2",
            versions=[AffectVersionVersionRequired(version="0.1.2", status=cdx.AffectedStatus.AFFECTED)],
        )
    ]

    other.affects = [
        Affect(
            ref="pkg:deb/debian/test-component-1@3.4.5",
            versions=[AffectVersionRangeRequired(range="vers:deb/>=3.0.0|<4.0.0", status=cdx.AffectedStatus.AFFECTED)],
        )
    ]

    vuln.merge(other)

    assert len(vuln.affects) == 2
