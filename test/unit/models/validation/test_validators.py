"""Test module for SBOM validation check models."""

from __future__ import annotations

from pathlib import Path

import pytest

from hoppr.exceptions import HopprValidationError
from hoppr.models.validation.code_climate import IssueList
from hoppr.models.validation.validators import SbomValidator, validate


@pytest.mark.parametrize(
    argnames=["sbom_id"],
    argvalues=[
        pytest.param("components", id="components"),
        pytest.param("invalid", id="invalid"),
        pytest.param("last-renewal", id="last-renewal"),
        pytest.param("licenses-field", id="licenses-field"),
        pytest.param("licenses-types", id="licenses-types"),
        pytest.param("merged", id="merged"),
        pytest.param("metadata-authors", id="metadata-authors"),
        pytest.param("metadata-only", id="metadata-only"),
        pytest.param("metadata-supplier", id="metadata-supplier"),
        pytest.param("metadata-timestamp", id="metadata-timestamp"),
        pytest.param("name-field", id="name-field"),
        pytest.param("name-or-id", id="name-or-id"),
        pytest.param("purchase-order", id="purchase-order"),
        pytest.param("sbom-components", id="sbom-components"),
        pytest.param("sbom-spec-version", id="sbom-spec-version"),
        pytest.param("sbom-unique-id", id="sbom-unique-id"),
        pytest.param("sbom-vulnerabilities-field", id="sbom-vulnerabilities-field"),
        pytest.param("supplier-field", id="supplier-field"),
        pytest.param("unique-id", id="unique-id"),
        pytest.param("valid", id="valid"),
        pytest.param("version-field", id="version-field"),
    ],
)
def test_validate(sbom_id: str, resources_dir: Path):
    """Test the `hoppr.models.validation.validators.validate` function."""
    validate_resources_dir = resources_dir.relative_to(Path.cwd()) / "validate"
    sbom_file = validate_resources_dir / "sboms" / f"validate-test-{sbom_id}.cdx.json"
    expected = IssueList.parse_file(validate_resources_dir / "code_climate" / f"expected-issues-{sbom_id}.json")

    assert validate(sbom_file) == expected


@pytest.mark.parametrize(
    argnames=["spec_version", "expected_results"],
    argvalues=[
        pytest.param("1.3", "Got specVersion 1.3; should be 1.5"),
        pytest.param("1.2", "Got specVersion 1.2; cannot be validated"),
        pytest.param("1.4", "Got specVersion 1.4; should be 1.5"),
        pytest.param("1.6", "Got an invalid specVersion of 1.6; should be 1.5"),
    ],
)
def test_invalid_spec_version(spec_version: str, expected_results: str):
    """Test for cases where the spec version is invalid."""
    with pytest.raises(
        expected_exception=HopprValidationError,
        match=expected_results,
    ):
        SbomValidator._validate_spec_version(spec_version)  # pylint: disable=protected-access
