"""Test module for pydantic models for `hopctl validate sbom` configuration file."""

from __future__ import annotations

import importlib
import pkgutil

import pytest

from hoppr.models.validation.checks import ValidateConfig


@pytest.fixture(name="validate_config")
def _validate_config() -> ValidateConfig:
    # Import module if not already imported, reload if it was
    module = importlib.import_module("hoppr.models.validation.checks")
    importlib.reload(module)

    return module.ValidateConfig()


def test_validate_config_default_config(validate_config: ValidateConfig):
    """Test ValidateConfig.default_config method"""
    default_config = pkgutil.get_data(package="hoppr.models.validation", resource="profiles/default.config.yml") or b""
    assert validate_config.yaml() == default_config.decode(), "default.config.yml needs to be regenerated"
