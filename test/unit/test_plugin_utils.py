"""Test module for `hoppr.plugin_utils`."""

from __future__ import annotations

from collections.abc import Callable
from typing import TYPE_CHECKING

import pytest

import hoppr.plugin_utils

from hoppr.result import Result


if TYPE_CHECKING:
    from os import PathLike

    from pytest import FixtureRequest, MonkeyPatch

    StrPath = str | PathLike[str]


@pytest.fixture(name="which_fixture")
def _which_fixture(request: FixtureRequest) -> Callable[[StrPath, int, StrPath | None], StrPath | None]:
    """Fixture used to patch `shutil.which`."""

    def _which(cmd: StrPath, _mode: int = 1, _path: StrPath | None = None) -> StrPath | None:
        return dict(
            getattr(request, "param", {"alpha": "alpha", "beta": "beta", "gamma": "gamma", "delta": "delta"})
        ).get(cmd)

    return _which


@pytest.mark.parametrize(
    argnames=["repo_url", "expected_dir_name"],
    argvalues=[
        pytest.param(
            "http://archive.ubuntu.com/ubuntu",
            "http%3A%2F%2Farchive.ubuntu.com%2Fubuntu",
            id="ubuntu-archive-url",
        ),
        pytest.param(
            "https://crates.io/api/v1/crates",
            "https%3A%2F%2Fcrates.io%2Fapi%2Fv1%2Fcrates",
            id="crates.io-url",
        ),
        pytest.param(
            "https://dl.rockylinux.org/vault/rocky/8.6/BaseOS/x86_64/os",
            "https%3A%2F%2Fdl.rockylinux.org%2Fvault%2Frocky%2F8.6%2FBaseOS%2Fx86_64%2Fos",
            id="rocky-baseos-url",
        ),
        pytest.param(
            "https://repo.maven.apache.org/maven2/",
            "https%3A%2F%2Frepo.maven.apache.org%2Fmaven2",
            id="maven-repo-url",
        ),
        pytest.param(
            "https://www.nuget.org/api/v2/package",
            "https%3A%2F%2Fwww.nuget.org%2Fapi%2Fv2%2Fpackage",
            id="nuget-api-url",
        ),
        pytest.param(
            "file://",
            "file%3A%2F%2F",
            id="ubuntu-archive-url",
        ),
    ],
)
def test_dir_name_from_repo_url(repo_url: str, expected_dir_name: str):
    """Test `hoppr.plugin_utils.dir_name_from_repo_url` function."""
    assert hoppr.plugin_utils.dir_name_from_repo_url(repo_url) == expected_dir_name


@pytest.mark.parametrize(
    argnames=["dir_name", "expected_repo_url"],
    argvalues=[
        pytest.param(
            "http%3A%2F%2Farchive.ubuntu.com%2Fubuntu",
            "http://archive.ubuntu.com/ubuntu",
            id="ubuntu-archive-url",
        ),
        pytest.param(
            "https%3A%2F%2Fcrates.io%2Fapi%2Fv1%2Fcrates",
            "https://crates.io/api/v1/crates",
            id="crates.io-url",
        ),
        pytest.param(
            "https%3A%2F%2Fdl.rockylinux.org%2Fvault%2Frocky%2F8.6%2FBaseOS%2Fx86_64%2Fos",
            "https://dl.rockylinux.org/vault/rocky/8.6/BaseOS/x86_64/os",
            id="rocky-baseos-url",
        ),
        pytest.param(
            "https%3A%2F%2Frepo.maven.apache.org%2Fmaven2%2F",
            "https://repo.maven.apache.org/maven2/",
            id="maven-repo-url",
        ),
        pytest.param(
            "https%3A%2F%2Fwww.nuget.org%2Fapi%2Fv2%2Fpackage",
            "https://www.nuget.org/api/v2/package",
            id="nuget-api-url",
        ),
    ],
)
def test_repo_url_from_dir_name(dir_name: str, expected_repo_url: str):
    """Test `hoppr.plugin_utils.repo_url_from_dir_name` function."""
    assert hoppr.plugin_utils.repo_url_from_dir_name(dir_name) == expected_repo_url


@pytest.mark.parametrize(
    argnames=["which_fixture", "expected_result"],
    argvalues=[
        pytest.param(
            {"alpha": None, "beta": "beta", "gamma": None, "delta": "delta"},
            Result.fail(
                message="The following required commands are unavailable: alpha, gamma. Please install and try again."
            ),
            id="missing-alpha-gamma",
        ),
        pytest.param(
            {"alpha": "alpha", "beta": None, "gamma": "gamma", "delta": None},
            Result.fail(
                message="The following required commands are unavailable: beta, delta. Please install and try again."
            ),
            id="missing-beta-delta",
        ),
        pytest.param(
            {"alpha": "alpha", "beta": "beta", "gamma": "gamma", "delta": "delta"},
            Result.success(),
            id="no-missing-cmds",
        ),
    ],
    indirect=["which_fixture"],
)
def test_check_for_missing_cmds(
    monkeypatch: MonkeyPatch,
    which_fixture: Callable[[StrPath, int, StrPath | None], StrPath | None],
    expected_result: Result,
):
    """Test `hoppr.plugin_utils.check_for_missing_commands` function."""
    monkeypatch.setattr(target=hoppr.plugin_utils, name="which", value=which_fixture)

    test_cmds: list[list[str | list[str]]] = [
        ["alpha", "beta", "gamma", "delta"],
        ["alpha", "beta", ["gamma", "delta"]],
        ["alpha", ["beta", "gamma"], "delta"],
        [["alpha", "beta"], "gamma", "delta"],
        [["alpha"], "beta", ["gamma"], "delta"],
        [["alpha", "beta", "gamma", "delta"]],
    ]

    for test_cmd in test_cmds:
        assert hoppr.plugin_utils.check_for_missing_commands(test_cmd) == expected_result
