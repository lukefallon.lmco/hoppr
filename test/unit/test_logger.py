"""
Test module for HopprLogger class
"""
import logging
import re

from pathlib import Path

import pytest

from pytest import FixtureRequest

from hoppr.logger import HopprLogger

LOG_PATTERNS = [
    r".*\[INFO\].*test info message.*",
    r".*\[WARNING\].*test warning message.*",
    r".*\[ERROR\].*test error message.*",
    r".*\[CRITICAL\].*test critical message.*",
]


@pytest.fixture(name="logger")
def logger_fixture(request: FixtureRequest, tmp_path: Path) -> HopprLogger:
    """
    Fixture to return HopprLogger instance
    """
    param_dict = dict(getattr(request, "param", {}))

    # Set default params for HopprLogger
    param_dict["filename"] = tmp_path / param_dict.get("filename", "hoppr1.log")
    param_dict["name"] = param_dict.get("name")
    param_dict["level"] = param_dict.get("level", logging.INFO)
    param_dict["flush_immed"] = param_dict.get("flush_immed", False)

    logger = HopprLogger(**param_dict)
    logger.debug("test debug message")
    logger.info("test info message")
    logger.warning("test warning message")
    logger.error("test error message")
    logger.critical("test critical message")

    return logger


@pytest.mark.parametrize(argnames="logger", argvalues=[{"filename": "hoppr1.log"}], indirect=True)
def test_logging(logger: HopprLogger):
    """
    Test log methods at INFO log level
    """
    logger.flush()
    logger.close()

    captured = Path(logger.filename).read_text(encoding="utf-8")
    assert re.match(pattern="\n".join(LOG_PATTERNS), string=captured)


@pytest.mark.parametrize(
    argnames="logger",
    argvalues=[{"filename": "hoppr1.log", "level": logging.DEBUG}],
    indirect=True,
)
def test_verbose_logging(logger: HopprLogger):
    """
    Test log methods at DEBUG log level
    """
    logger.flush()
    logger.close()

    captured = Path(logger.filename).read_text(encoding="utf-8")
    assert re.match(pattern="\n".join([r".*\[DEBUG\].*test debug message.*", *LOG_PATTERNS]), string=captured)


@pytest.mark.parametrize(argnames="logger", argvalues=[{"filename": "hoppr2.log"}], indirect=True)
def test_clear_log(logger: HopprLogger):
    """
    Test HopprLogger.clear_targets method
    """
    logger.clear_targets()
    logger.close()

    captured = Path(logger.filename).read_text(encoding="utf-8")
    assert re.match(pattern=r"\s*", string=captured)


@pytest.mark.parametrize(argnames="logger", argvalues=[{"filename": "hoppr1.log"}], indirect=True)
def test_logging_nolock(logger: HopprLogger):
    """
    Test log methods with no lock
    """
    logger.flush()
    logger.close()

    captured = Path(logger.filename).read_text(encoding="utf-8")
    assert re.match(pattern="\n".join(LOG_PATTERNS), string=captured)


@pytest.mark.parametrize(
    argnames="logger",
    argvalues=[{"filename": "hoppr2.log", "flush_immed": True}],
    indirect=True,
)
def test_clear_log_nolock(logger: HopprLogger):
    """
    Test HopprLogger.clear_targets method with no lock
    """
    logger.clear_targets()
    logger.close()

    captured = Path(logger.filename).read_text(encoding="utf-8")
    assert re.match(pattern=r"\s*", string=captured)
