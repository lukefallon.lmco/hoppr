"""
Test module for Result class
"""

# pylint: disable=protected-access

from typing import Callable

import pytest
import requests

from hoppr.exceptions import HopprError
from hoppr.result import Result


@pytest.mark.parametrize(
    argnames=["result_method", "result_message", "check_method"],
    argvalues=[
        pytest.param(Result.success, "", Result.is_success, id="success"),
        pytest.param(Result.retry, "Try again", Result.is_retry, id="retry"),
        pytest.param(Result.fail, "Sorry, Charlie", Result.is_fail, id="fail"),
        pytest.param(Result.warn, "Shouldn't have done that", Result.is_warn, id="warn"),
        pytest.param(Result.skip, "N/A", Result.is_skip, id="skip"),
        pytest.param(Result.excluded, "Excluded result", Result.is_excluded, id="excluded"),
    ],
)
def test_result(result_method: Callable[[str], Result], result_message: str, check_method: Callable[[Result], bool]):
    """
    Test Result convenience methods
    """
    result = result_method(result_message)

    assert result.message == result_message
    assert check_method(result)
    assert str(result) == f"{result.status.name}{f', msg: {result_message}' if result_message else ''}"


@pytest.mark.parametrize(
    argnames=["response_fixture", "check_method"],
    argvalues=[
        ({"status_code": 200, "content": b"success"}, Result.is_success),
        ({"status_code": 404, "content": b"not found"}, Result.is_fail),
        ({"status_code": 500, "content": b"server error"}, Result.is_retry),
    ],
    ids=[
        "OK (200)",
        "Not Found (404)",
        "Internal Server Error (500)",
    ],
    indirect=["response_fixture"],
)
def test_from_http_response(response_fixture: Callable[..., requests.Response], check_method: Callable[[Result], bool]):
    """
    Test Result.from_http_response method
    """
    response = response_fixture()
    result = Result.from_http_response(response)

    assert check_method(result)
    assert result.message == (
        f"HTTP Status Code: {response.status_code}{'' if result.is_success() else f'; {response.content.decode()}'}"
    )


@pytest.mark.parametrize(
    argnames=[
        "result",
        "other",
        "expected_message",
        "check_method",
    ],
    argvalues=[
        (
            Result.success(),
            Result.fail("this failed"),
            "this failed",
            Result.is_fail,
        ),
        (
            Result.success("it worked"),
            Result.skip("this failed"),
            "it worked",
            Result.is_success,
        ),
        (
            Result.success("it worked"),
            Result.fail("this failed", return_obj="fail_obj"),
            "it worked\nthis failed",
            Result.is_fail,
        ),
    ],
)
def test_merge(result: Result, other: Result, expected_message: str, check_method: Callable[[Result], bool]):
    """
    Test Result.merge method
    """
    result.merge(other)

    assert check_method(result)
    assert result.message == expected_message


def test_merge_exception():
    """
    Test Result.merge method raises HopprError
    """
    result = Result.success("it worked", return_obj="init obj")
    other = Result.fail("this failed", return_obj="fail_obj")

    with pytest.raises(HopprError):
        result.merge(other)
