"""
Test module for HopprPlugin class
"""

# pylint: disable=redefined-outer-name
# pylint: disable=unused-argument

from __future__ import annotations

from subprocess import CompletedProcess

import pytest

from pytest import FixtureRequest, MonkeyPatch

import hoppr.utils

from hoppr.base_plugins.hoppr import subprocess  # type: ignore[attr-defined]
from hoppr.base_plugins.hoppr import HopprPlugin, hoppr_process, hoppr_rerunner
from hoppr.exceptions import HopprError
from hoppr.models import HopprContext
from hoppr.models.sbom import Component
from hoppr.result import Result


class MyPlugin(HopprPlugin):
    """HopprPlugin subclass for testing"""

    supported_purl_types = ["good1", "good2"]

    def __init__(self, context: HopprContext, config: dict | None):
        super().__init__(context=context, config=config)

        self.proc_comp_count = 0  # for counting the number of times process_component is called
        self.finalize_count = 0  # for counting the number of times post_stage_process is called

    def get_version(self) -> str:
        return "1.70.1"

    @hoppr_rerunner
    def wrong_return_obj(self) -> str:
        """
        Method to test unsupported `Result.return_obj` type
        """
        return self.get_version()

    @hoppr_process
    @hoppr_rerunner
    def process_component(self, comp: Component) -> Result:
        self.proc_comp_count += 1
        if self.config == {"config": "THROW_EXCEPTION"}:
            raise HopprError("Throwing exception as specified by config value")
        return Result.retry(
            "my_plugin TestBasePlugin method get_component failed, but might succeed "
            "on a subsequent attempt if the anti-matter injectors come on-line."
        )

    @hoppr_process
    @hoppr_rerunner
    def post_stage_process(self) -> Result:
        self.finalize_count += 1
        return Result.skip()


class MyNoCompPlugin(HopprPlugin):
    """HopprPlugin subclass for testing"""

    supported_purl_types = ["good1", "good2"]

    def get_version(self):
        return "1.70.1"


@pytest.fixture(name="component")
def component_fixture(request: FixtureRequest):
    """
    Test Component fixture
    """
    purl_string = getattr(request, "param", "pkg:good1/something/else@1.2.3")
    purl = hoppr.utils.get_package_url(purl_string)
    return Component.parse_obj({"name": purl.name, "version": purl.version, "purl": purl_string, "type": "file"})


@pytest.fixture(scope="function")
def no_comp_plugin_fixture(context_fixture: HopprContext, config_fixture: dict[str, str]) -> MyNoCompPlugin:
    """
    Fixture to return MyNoCompPlugin
    """
    return MyNoCompPlugin(context=context_fixture, config=config_fixture)


@pytest.mark.parametrize(argnames="config_fixture", argvalues=[{"config": "CONFIG"}], indirect=True)
def test_plugin_base(plugin_fixture: MyPlugin, component: Component):
    """
    Test HopprPlugin base functionality
    """
    assert plugin_fixture.supports_purl_type("good1") is True
    assert plugin_fixture.supports_purl_type("good2") is True
    assert plugin_fixture.supports_purl_type("bad") is False

    assert plugin_fixture.config == {"config": "CONFIG"}

    collect_result = plugin_fixture.process_component(comp=component)
    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert plugin_fixture.proc_comp_count == 3

    post_stage_process_result = plugin_fixture.post_stage_process()
    assert post_stage_process_result.is_skip(), f"Expected SKIP result, got {post_stage_process_result}"
    assert plugin_fixture.finalize_count == 1


@pytest.mark.parametrize(argnames="config_fixture", argvalues=[{"config": "THROW_EXCEPTION"}], indirect=True)
@pytest.mark.parametrize(argnames="component", argvalues=["pkg:good1/something/else@1.2.3"], indirect=True)
def test_plugin_base_exception(plugin_fixture: MyPlugin, component: Component):
    """
    Test MyPlugin.process_component method raises HopprError
    """
    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_fail()


@pytest.mark.parametrize(argnames="component", argvalues=["pkg:bad/something/else@1.2.3"], indirect=True)
def test_plugin_base_bad_purl_type(plugin_fixture: MyPlugin, component: Component):
    """
    Test MyPlugin.process_component method with unsupported purl type
    """
    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_skip()


@pytest.mark.parametrize(argnames="component", argvalues=["pkg:good1/something/else@1.2.3"], indirect=True)
def test_plugin_base_bad_command(plugin_fixture: MyPlugin, component: Component):
    """
    Test MyPlugin.process_component method with nonexistent required command
    """
    plugin_fixture.required_commands = ["doesnotexistcommand"]

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_fail()


@pytest.mark.parametrize(argnames="component", argvalues=["pkg:good1/something/else@1.2.3"], indirect=True)
def test_plugin_base_default_iml(no_comp_plugin_fixture: MyNoCompPlugin, component: Component):
    """
    Test base HopprPlugin.process_component and post_stage_process methods
    """
    collect_result = no_comp_plugin_fixture.process_component(component)
    assert collect_result.is_skip()

    post_stage_process_result = no_comp_plugin_fixture.post_stage_process()
    assert post_stage_process_result.is_skip()


@pytest.mark.parametrize(argnames="component", argvalues=["pkg:good1/something/else@1.2.3"], indirect=True)
def test_plugin_base_no_component_purl(no_comp_plugin_fixture: MyNoCompPlugin, component: Component):
    """
    Test base HopprPlugin.process_component method with component purl of None
    """
    component.purl = None
    collect_result = no_comp_plugin_fixture.process_component(component)
    assert collect_result.is_fail()
    assert collect_result.message == "No purl supplied for component"


@pytest.mark.parametrize(
    argnames="completed_process_fixture",
    argvalues=[{"returncode": 0}, {"returncode": 1}],
    indirect=True,
)
def test_plugin_run_command(
    plugin_fixture: MyPlugin,
    completed_process_fixture: CompletedProcess,
    monkeypatch: MonkeyPatch,
):
    """
    Test base HopprPlugin.run_command method
    """
    monkeypatch.setattr(target=subprocess, name="run", value=lambda *args, **kwargs: completed_process_fixture)

    plugin_fixture.run_command(command=["HopprPlugin", "test", "command"])


@pytest.mark.parametrize(argnames="config_fixture", argvalues=[{"process_timeout": 60}], indirect=True)
def test_plugin_run_command_timeout(plugin_fixture: MyPlugin, monkeypatch: MonkeyPatch):
    """
    Test MyPlugin.run_command method with expired timeout
    """

    def _subprocess_run_patch(*_, **__):
        raise subprocess.TimeoutExpired(["test"], 42)

    monkeypatch.setattr(target=subprocess, name="run", value=_subprocess_run_patch)

    result = plugin_fixture.run_command(["anything"])

    assert result.returncode == 124


def test_plugin_observer_pattern(plugin_fixture: MyPlugin):
    """
    Test MyPlugin observer pattern methods: subscribe, notify, unsubscribe
    """
    observer = object()
    callback_data = ""

    def _callback(data: str):
        nonlocal callback_data
        callback_data = data

    plugin_fixture.subscribe(observer=observer, callback=_callback)
    assert observer in plugin_fixture.observers

    plugin_fixture.notify("Test notify message")
    assert callback_data == "Test notify message"

    plugin_fixture.unsubscribe(observer=observer)
    assert observer not in plugin_fixture.observers


@pytest.mark.parametrize(argnames="component", argvalues=["pkg:good1/something/else@1.2.3"], indirect=True)
def test_retry_skip(plugin_fixture: MyPlugin, component: Component):
    """
    Test MyPlugin.post_stage_process method retry skip
    """
    result = plugin_fixture.post_stage_process()
    plugin_fixture.close_logger()

    assert result.is_skip()
    assert plugin_fixture.finalize_count == 1


@pytest.mark.parametrize(argnames="component", argvalues=["pkg:good1/something/else@1.2.3"], indirect=True)
def test_retry_fail(plugin_fixture: MyPlugin, component: Component):
    """
    Test MyPlugin.process_component method retry fail
    """
    result = plugin_fixture.process_component(component)
    plugin_fixture.close_logger()

    assert result.is_fail(), f"Expected FAIL result, got {result}"
    assert result.message == (
        "Failure after 3 attempts, final message my_plugin TestBasePlugin method get_component failed, "
        "but might succeed on a subsequent attempt if the anti-matter injectors come on-line."
    )


def test_retry_not_result(plugin_fixture: MyPlugin):
    """
    Test MyPlugin.get_version method fails when non-Result object returned
    """
    result = plugin_fixture.wrong_return_obj()
    plugin_fixture.close_logger()

    assert result.is_fail(), f"Expected FAIL result, got {result}"
    assert result.message == "Method wrong_return_obj returned str in rerunner. Result object required"


def test_pre_stage_process(plugin_fixture: MyPlugin):
    """
    Test MyPlugin.pre_stage_process method
    """
    result = plugin_fixture.pre_stage_process()
    assert result.is_skip(), f"Expected SKIP result, got {result}"
