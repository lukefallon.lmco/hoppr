"""
Test module for `hopctl merge` subcommand
"""

from __future__ import annotations

import json
import re

from pathlib import Path
from typing import TYPE_CHECKING, TypeAlias

import pytest

from typer.testing import CliRunner

import hoppr.cli.options
import hoppr.utils

from hoppr.cli import app
from hoppr.models.sbom import Sbom


if TYPE_CHECKING:
    LogCaptureFixture: TypeAlias = pytest.LogCaptureFixture
    MonkeyPatch: TypeAlias = pytest.MonkeyPatch


def test_hopctl_merge_logging_validation_error(  # pylint: disable=duplicate-code
    hopctl_runner: CliRunner,
    tmp_path: Path,
    caplog: LogCaptureFixture,
    monkeypatch: MonkeyPatch,
):
    """
    Test logging output of `hopctl merge` subcommand if a pydantic `ValidationError` is raised
    """
    # Set logger back to None so a new one gets created
    monkeypatch.setattr(target=hoppr.cli.options, name="_logger", value=None)

    sbom_data = {
        "bomFormat": "--CycloneDX--",  # Must be Literal["CycloneDX"]
        "specVersion": "1.4",
        "components": [{"name": "test-component", "version": "0.0.1"}],  # Missing `type` field
    }

    (tmp_path / "bad-sbom.cdx.json").write_text(data=json.dumps(sbom_data), encoding="utf-8")

    result = hopctl_runner.invoke(
        app,
        args=[
            "merge",
            f"--log={tmp_path / 'hoppr-pytest.log'}",
            f"--sbom={tmp_path / 'bad-sbom.cdx.json'}",
            f"--output-file={tmp_path / 'hopctl-merge-pytest.json'}",
        ],
        catch_exceptions=False,
    )

    output = re.sub(pattern=r"\s+│\n│\s", repl="", string=result.stdout)

    assert result.exit_code == 2
    assert "Invalid value for '-s' / '--sbom':" in output
    assert f"'{tmp_path / 'bad-sbom.cdx.json'}'" in output

    # Verify expected log output
    expected_log_message = f"""'{tmp_path / 'bad-sbom.cdx.json'}' failed to validate against the CycloneDX schema:
    2 validation errors for Sbom
    bomFormat
      unexpected value; permitted: 'CycloneDX' (type=value_error.const; given=--CycloneDX--; permitted=('CycloneDX',))
    components -> type
      field required (type=value_error.missing)"""

    *_, last_message = caplog.messages
    assert last_message == expected_log_message


def test_hopctl_merge_manifest_input(hopctl_runner: CliRunner, resources_dir: Path, tmp_path: Path):
    """
    Test `hopctl merge` with manifest file input
    """
    result = hopctl_runner.invoke(
        app,
        args=[
            "merge",
            f"--manifest={resources_dir / 'manifest' / 'unit' / 'manifest.yml'}",
            f"--output-file={tmp_path / 'hopctl-merge-pytest.json'}",
            f"--log={tmp_path / 'hoppr-pytest.log'}",
        ],
    )

    assert result.exit_code == 0


def test_hopctl_merge_missing_input_files(hopctl_runner: CliRunner, resources_dir: Path, tmp_path: Path):
    """
    Test `hopctl merge` with insufficient number of SBOM files
    """
    Sbom.loaded_sboms.clear()
    result = hopctl_runner.invoke(
        app,
        args=[
            "merge",
            f"--sbom={resources_dir / 'bom' / 'unit_bom1_mini.json'}",
            f"--output-file={tmp_path / 'hopctl-merge-pytest.json'}",
            f"--log={tmp_path / 'hoppr-pytest.log'}",
        ],
    )

    assert result.exit_code != 0
    assert "A minimum of two SBOM files must be provided" in result.stdout


def test_hopctl_merge_sbom_dir_input(hopctl_runner: CliRunner, resources_dir: Path, tmp_path: Path):
    """
    Test `hopctl merge` with directory of SBOM files
    """
    result = hopctl_runner.invoke(
        app,
        args=[
            "merge",
            f"--sbom-dir={resources_dir / 'bom'}",
            f"--output-file={tmp_path / 'hopctl-merge-pytest.json'}",
            f"--log={tmp_path / 'hoppr-pytest.log'}",
        ],
    )

    assert result.exit_code == 0


def test_hopctl_merge_sbom_dir_exception(hopctl_runner: CliRunner, tmp_path: Path, monkeypatch: MonkeyPatch):
    """
    Test `hopctl merge` with directory of SBOM files raises BadParameter exception
    """
    (tmp_path / "bad-sbom-1.cdx.json").write_text(data='{"invalid": ["sbom", "format"]}', encoding="utf-8")
    (tmp_path / "bad-sbom-2.cdx.json").write_text(data='{"invalid": ["sbom", "format"]}', encoding="utf-8")

    monkeypatch.setattr(target=Sbom, name="loaded_sboms", value={})

    result = hopctl_runner.invoke(
        app,
        args=[
            "merge",
            f"--sbom-dir={tmp_path}",
            f"--output-file={tmp_path / 'hopctl-merge-pytest.json'}",
            f"--log={tmp_path / 'hoppr-pytest.log'}",
        ],
        catch_exceptions=False,
    )

    output = re.sub(pattern=r"╭.*╮|╰.*╯|( ?│ ?)|\n|\s{2,}", repl="", string=result.stdout)

    assert result.exit_code != 0
    assert (
        "Invalid value: A minimum of two SBOM files must be provided via "
        "the --sbom,--sbom-dir, --sbom-url, and --manifest arguments."
    ) in output


def test_hopctl_merge_sbom_input(
    hopctl_runner: CliRunner,
    resources_dir: Path,
    monkeypatch: MonkeyPatch,
    tmp_path: Path,
):
    """
    Test `hopctl merge` with SBOM files input
    """
    monkeypatch.setattr(target=hoppr.utils, name="is_basic_terminal", value=lambda: False)

    result = hopctl_runner.invoke(
        app,
        args=[
            "merge",
            f"--sbom={resources_dir / 'bom' / 'unit_bom1_mini.json'}",
            f"--sbom={resources_dir / 'bom' / 'unit_bom2_mini.json'}",
            f"--sbom={resources_dir / 'bom' / 'unit_bom3_mini.json'}",
            f"--output-file={tmp_path / 'hopctl-merge-pytest.json'}",
            f"--log={tmp_path / 'hoppr-pytest.log'}",
        ],
    )

    assert result.exit_code == 0
