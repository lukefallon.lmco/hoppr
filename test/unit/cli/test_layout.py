"""
Test module for the Hoppr `layout` module
"""

# pylint: disable=protected-access

from __future__ import annotations

import warnings

from typing import Any

import pytest

from pytest import CaptureFixture, MonkeyPatch
from rich.console import Console, RenderableType
from rich.progress import Progress, Task, TaskID
from rich.text import Text

import hoppr.utils

from hoppr.cli.layout import HopprConsole, HopprJobsPanel, HopprLayout, HopprOutputPanel, HopprSpinnerColumn


def test_hoppr_jobs_panel_add_task(monkeypatch: MonkeyPatch):
    """
    Test the `HopprJobsPanel.add_task` method
    """

    dummy_description = "Test description"
    dummy_fields = {"fields": {"status": "[yellow]:pause_button:", "something_else": 1234}}

    progress_add_task_called = False

    def _mock_progress_add_task(  # pylint: disable=too-many-arguments, unused-argument
        self,
        description: str,
        start: bool = True,
        total: float | None = 100.0,
        completed: int = 0,
        visible: bool = True,
        **fields: Any,
    ) -> TaskID:
        nonlocal progress_add_task_called
        progress_add_task_called = True

        assert description == dummy_description
        assert fields == dummy_fields

        return TaskID(0)

    with monkeypatch.context() as patch:
        patch.setattr(target=Progress, name="add_task", value=_mock_progress_add_task)

        jobs_panel = HopprJobsPanel()
        jobs_panel.add_task(dummy_description, total=1, **dummy_fields)

        assert progress_add_task_called


def test_hoppr_output_panel_print(monkeypatch: MonkeyPatch):
    """
    Test the `HopprOutputPanel.print` method
    """

    dummy_str = "Dummy str"

    console_append_called = False

    def _mock_console_append(_self, _: RenderableType):
        nonlocal console_append_called
        console_append_called = True

    with monkeypatch.context() as patch:
        patch.setattr(target=HopprConsole, name="append", value=_mock_console_append)

        output_panel = HopprOutputPanel()

        output_panel.print(dummy_str)

        assert console_append_called


def test_hoppr_layout_add_job():
    """
    Test the `HopprLayout.add_job` method
    """

    dummy_description = "Testing 1, 2, 5"
    dummy_fields = {"fields": {"some_field": "hello", "another_field": "goodbye"}}

    jobs_panel_add_task_called = False

    class _MockHopprJobsPanel(HopprJobsPanel):
        # pylint: disable=too-few-public-methods
        def add_task(self, description: str, total: int = 1, **fields) -> TaskID:
            nonlocal jobs_panel_add_task_called
            jobs_panel_add_task_called = True

            assert description == dummy_description
            assert fields == dummy_fields

            return TaskID(0)

    hopper_layout = HopprLayout()
    hopper_layout.jobs_panel = _MockHopprJobsPanel()

    hopper_layout.add_job(dummy_description, total=1, **dummy_fields)

    assert jobs_panel_add_task_called


def test_hoppr_layout_advance_job_not_found():
    """
    Test the `HopprLayout.advance_job` method if job is not found
    """
    hoppr_layout = HopprLayout()
    hoppr_layout.add_job(description="pytest")

    assert hoppr_layout.advance_job("not_found") is None


def test_hoppr_layout_is_job_finished():
    """
    Test the `HopprLayout.is_job_finished` method
    """
    job_name = "HopprTestJob"
    hoppr_layout = HopprLayout()
    hoppr_layout.add_job(job_name)
    hoppr_layout.start_job(job_name)

    # Test that calling with a nonexistent job name returns True
    assert hoppr_layout.is_job_finished("not_found") is True

    assert hoppr_layout.is_job_finished(job_name) is False

    hoppr_layout.advance_job(job_name)
    hoppr_layout.stop_job(job_name)

    assert hoppr_layout.is_job_finished(job_name) is True


@pytest.mark.parametrize(
    argnames=["basic_term", "expected_output"],
    argvalues=[
        pytest.param(False, "HopprOutputPanel.print called", id="full_terminal"),
        pytest.param(True, "console.print called", id="basic_terminal"),
    ],
)
def test_hoppr_layout_print(basic_term: bool, expected_output: str, capsys: CaptureFixture, monkeypatch: MonkeyPatch):
    """
    Test the `HopprLayout.print` method
    """
    console = Console()

    monkeypatch.setattr(target=hoppr.utils, name="is_basic_terminal", value=lambda: basic_term)
    monkeypatch.setattr(
        target=HopprOutputPanel, name="print", value=lambda *_, **__: console.print("HopprOutputPanel.print called")
    )

    hoppr_layout = HopprLayout()
    hoppr_layout.print("console.print called")

    output = capsys.readouterr()
    assert output.out.strip() == expected_output


def test_hoppr_layout_update_job():
    """
    Test the `HopprLayout.update_job` method
    """
    hoppr_layout = HopprLayout()
    task_id = hoppr_layout.add_job(description="pytest", status="success")
    hoppr_layout.update_job(name="pytest", status="fail")

    assert hoppr_layout.update_job("not_found") is None
    assert hoppr_layout.job_id_map.get("pytest") is not None
    assert hoppr_layout.jobs_panel.progress_bar.tasks[task_id].fields["status"] == "fail"


def test_hoppr_layout_warning(monkeypatch: MonkeyPatch):
    """
    Test raising a warning gets printed to output console panel
    """
    hoppr_layout = HopprLayout()

    def _layout_print(msg: str, source: str, *_, **__):
        assert msg == "pytest"
        assert source == "[bold yellow]UserWarning"

    monkeypatch.setattr(target=hoppr_layout, name="print", value=_layout_print)

    warnings.warn(message="pytest", category=UserWarning)


@pytest.mark.parametrize(
    argnames=["status", "expected_text"],
    argvalues=[
        pytest.param("fail", Text.from_markup(HopprSpinnerColumn._FAIL_X)),
        pytest.param("success", Text.from_markup(HopprSpinnerColumn._SUCCESS_CHECK)),
        pytest.param("warn", Text.from_markup(HopprSpinnerColumn._WARNING_SIGN)),
    ],
    ids=["fail", "success", "warn"],
)
def test_hoppr_spinner_column_render(status: str, expected_text: Text):
    """
    Test the `HopprSpinnerColumn.render` method
    """
    task = Task(
        id=TaskID(999),
        total=None,
        completed=0,
        _get_time=lambda: 0,
        description="pytest",
        fields={"status": status},
    )

    hoppr_spinner_column = HopprSpinnerColumn()
    hoppr_spinner_column.render(task)

    assert hoppr_spinner_column.finished_text == expected_text
