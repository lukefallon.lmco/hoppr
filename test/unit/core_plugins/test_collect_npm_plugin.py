"""
Test module for CollectNpmPlugin class
"""

# pylint: disable=unused-argument
# pylint: disable=redefined-outer-name

from pathlib import Path
from typing import Callable

import pytest

from pytest import MonkeyPatch

import hoppr.plugin_utils

from hoppr.core_plugins.collect_npm_plugin import CollectNpmPlugin, requests  # type: ignore[attr-defined]
from hoppr.models.credentials import CredentialRequiredService, Credentials
from hoppr.models.manifest import Repository
from hoppr.models.sbom import Component
from hoppr.models.types import PurlType
from hoppr.result import Result

test_comp = Component(
    name="TestComponent",
    purl="pkg:npm/async@1.5.2",
    type="file",  # type: ignore[arg-type]
)


@pytest.fixture(name="component")
def component_fixture() -> Component:
    """
    Return test_comp fixture
    """
    return test_comp


@pytest.fixture
def plugin_fixture(plugin_fixture: CollectNpmPlugin, monkeypatch: MonkeyPatch, tmp_path: Path) -> CollectNpmPlugin:
    """
    Override and parametrize plugin_fixture to return CollectNpmPlugin
    """
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=lambda comp: ["https://url1.com"])

    plugin_fixture.context.repositories[PurlType.NPM] = [
        Repository.parse_obj({"url": "https://url2.com", "description": ""})
    ]

    return plugin_fixture


@pytest.mark.parametrize(
    argnames="response_fixture", argvalues=[{"status_code": 200, "content": b"0" * 1024}], indirect=True
)
def test_collect_npm(
    plugin_fixture: CollectNpmPlugin,
    response_fixture: Callable[[str], requests.Response],
    find_credentials_fixture: CredentialRequiredService,
    component: Component,
    monkeypatch: MonkeyPatch,
):
    """
    Test npm collector successful run with a given url
    """
    with monkeypatch.context() as patch:
        patch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
        patch.setattr(target=requests, name="get", value=response_fixture)

        collect_result = plugin_fixture.process_component(component)

    assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}."


RETRY_MESSAGE_TEMPLATE = "Failed to locate npm package for {purl}, return_code={status_code}"


@pytest.mark.parametrize(
    argnames=["response_fixture", "expected_return_status", "expected_msg_template"],
    argvalues=[
        ({"status_code": 401}, 401, RETRY_MESSAGE_TEMPLATE),
        ({"status_code": 403}, 403, RETRY_MESSAGE_TEMPLATE),
        ({"status_code": 404}, 404, RETRY_MESSAGE_TEMPLATE),
        ({"status_code": 500}, 500, RETRY_MESSAGE_TEMPLATE),
        ({"status_code": 502}, 502, RETRY_MESSAGE_TEMPLATE),
    ],
    ids=[
        "Unauthorized (401)",
        "Forbidden (403)",
        "Not Found (404)",
        "Internal Server Error (500)",
        "Bad Gateway (502)",
    ],
    indirect=["response_fixture"],
)
def test_collect_npm_fail(  # pylint: disable=too-many-arguments
    plugin_fixture: CollectNpmPlugin,
    response_fixture: Callable[[str], requests.Response],
    find_credentials_fixture: CredentialRequiredService,
    component: Component,
    expected_msg_template: str,
    expected_return_status: int,
    monkeypatch: MonkeyPatch,
):
    """
    Test npm collector unsuccessful run with a given url
    """
    # pylint: disable=duplicate-code
    with monkeypatch.context() as patch:
        patch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
        patch.setattr(target=requests, name="get", value=response_fixture)

        collect_result = plugin_fixture.process_component(component)

        assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
        assert collect_result.message.endswith(
            expected_msg_template.format(purl=component.purl, status_code=expected_return_status)
        )


def test_get_version(plugin_fixture: CollectNpmPlugin):
    """
    Test npm collector has version
    """
    assert len(plugin_fixture.get_version()) > 0
