"""
Test module for CollectCargoPlugin class
"""

# pylint: disable=redefined-outer-name

from typing import Callable

import pytest

from pytest import MonkeyPatch

from hoppr.core_plugins.collect_cargo_plugin import CollectCargoPlugin, requests  # type: ignore[attr-defined]
from hoppr.models.credentials import CredentialRequiredService, Credentials
from hoppr.models.manifest import Repository
from hoppr.models.sbom import Component
from hoppr.models.types import PurlType

test_component = Component(
    name="TestComponent",
    purl="pkg:cargo/example-package@1.2.3",
    type="file",  # type: ignore[arg-type]
)


@pytest.fixture(name="component")
def component_fixture() -> Component:
    """
    Test Component fixture
    """
    return test_component


@pytest.fixture
def plugin_fixture(plugin_fixture: CollectCargoPlugin, monkeypatch: MonkeyPatch) -> CollectCargoPlugin:
    """
    Override and parametrize plugin_fixture to return CollectCargoPlugin
    """
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=lambda comp: ["https://somewhere1.com"])

    plugin_fixture.context.repositories[PurlType.CARGO] = [
        Repository.parse_obj({"url": "https://somewhere2.com", "description": ""})
    ]

    return plugin_fixture


@pytest.mark.parametrize(
    argnames="response_fixture",
    argvalues=[{"status_code": 200, "content": b"0" * 1024}],
    indirect=True,
)
def test_collect_cargo(
    plugin_fixture: CollectCargoPlugin,
    find_credentials_fixture: CredentialRequiredService,
    response_fixture: Callable[..., requests.Response],
    component: Component,
    monkeypatch: MonkeyPatch,
):
    """
    Test Cargo collector successful run with a given URL
    """
    # pylint: disable=duplicate-code
    with monkeypatch.context() as patch:
        patch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
        patch.setattr(target=requests, name="get", value=response_fixture)

        collect_result = plugin_fixture.process_component(component)

    assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}."


@pytest.mark.parametrize(
    argnames=["response_fixture", "expected_status_code"],
    argvalues=[
        ({"status_code": 401}, 401),
        ({"status_code": 403}, 403),
        ({"status_code": 404}, 404),
        ({"status_code": 500}, 500),
        ({"status_code": 502}, 502),
    ],
    ids=[
        "Unauthorized (401)",
        "Forbidden (403)",
        "Not Found (404)",
        "Internal Server Error (500)",
        "Bad Gateway (502)",
    ],
    indirect=["response_fixture"],
)
def test_collect_cargo_fail(  # pylint: disable=too-many-arguments
    plugin_fixture: CollectCargoPlugin,
    find_credentials_fixture: CredentialRequiredService,
    response_fixture: Callable[..., requests.Response],
    component: Component,
    expected_status_code: int,
    monkeypatch: MonkeyPatch,
):
    """
    Test cargo collector unsuccessful run with a given url
    """
    # pylint: disable=duplicate-code
    with monkeypatch.context() as patch:
        patch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
        patch.setattr(target=requests, name="get", value=response_fixture)

        collect_result = plugin_fixture.process_component(component)

        assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
        assert collect_result.message == (
            f"Failed to download Cargo package pkg:cargo/example-package@1.2.3, status_code={expected_status_code}"
        )


def test_get_version(plugin_fixture: CollectCargoPlugin):
    """
    Test cargo collector has version
    """
    assert len(plugin_fixture.get_version()) > 0
