"""
Test module for CollectGolangPlugin class
"""

# pylint: disable=redefined-outer-name
# pylint: disable=unused-argument

from pathlib import Path
from typing import Callable

import pytest
import requests

from pytest import MonkeyPatch

import hoppr.plugin_utils

from hoppr.core_plugins.collect_golang_plugin import CollectGolangPlugin
from hoppr.models.credentials import CredentialRequiredService, Credentials
from hoppr.models.manifest import Repository
from hoppr.models.sbom import Component
from hoppr.models.types import PurlType
from hoppr.result import Result

test_component = Component(
    name="TestComponent",
    purl="pkg:golang/example/package@1.2.3",
    type="file",  # type: ignore[arg-type]
)


# pylint: disable=duplicate-code
@pytest.fixture(name="component")
def component_fixture() -> Component:
    """
    Test Component fixture
    """
    return test_component


@pytest.fixture
def plugin_fixture(
    plugin_fixture: CollectGolangPlugin, monkeypatch: MonkeyPatch, tmp_path: Path
) -> CollectGolangPlugin:
    """
    Override and parametrize plugin_fixture to return CollectGolangPlugin
    """
    # pylint: disable=duplicate-code
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=lambda comp: ["https://somewhere1.com"])

    plugin_fixture.context.repositories[PurlType.GOLANG] = [
        Repository.parse_obj({"url": "https://somewhere2.com", "description": ""})
    ]

    return plugin_fixture


@pytest.mark.parametrize(
    argnames="response_fixture", argvalues=[{"status_code": 200, "content": b"0" * 1024}], indirect=True
)
def test_collect_golang(
    plugin_fixture: CollectGolangPlugin,
    find_credentials_fixture: CredentialRequiredService,
    response_fixture: Callable[[str], requests.Response],
    component: Component,
    monkeypatch: MonkeyPatch,
):
    """
    Test golang collector successful run with a given url
    """

    with monkeypatch.context() as patch:
        patch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
        patch.setattr(target=requests, name="get", value=response_fixture)

        collect_result = plugin_fixture.process_component(component)

        assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}."


@pytest.mark.parametrize(
    argnames=["response_fixture", "mock_response_code"],
    argvalues=[
        ({"status_code": 401}, 401),
        ({"status_code": 403}, 403),
        ({"status_code": 404}, 404),
        ({"status_code": 500}, 500),
        ({"status_code": 502}, 502),
    ],
    ids=[
        "Unauthorized (401)",
        "Forbidden (403)",
        "Not Found (404)",
        "Internal Server Error (500)",
        "Bad Gateway (502)",
    ],
    indirect=["response_fixture"],
)
# pylint: disable=too-many-arguments
def test_collect_golang_fail(
    plugin_fixture: CollectGolangPlugin,
    find_credentials_fixture: CredentialRequiredService,
    response_fixture: Callable[[str], requests.Response],
    component: Component,
    monkeypatch: MonkeyPatch,
    mock_response_code: int,
):
    """
    Test unsuccessful golang collector run with a given url
    """

    with monkeypatch.context() as patch:
        patch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
        patch.setattr(target=requests, name="get", value=response_fixture)

        collect_result = plugin_fixture.process_component(component)

        assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
        assert collect_result.message == (
            f"Failed to locate Golang package for pkg:golang/example/package@1.2.3, return_code={mock_response_code}"
        )


def test_get_version(plugin_fixture: CollectGolangPlugin):
    """
    Test golang collector has version
    """
    assert len(plugin_fixture.get_version()) > 0
