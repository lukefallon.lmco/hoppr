"""
Test module for CollectNugetPlugin class
"""

# pylint: disable=redefined-outer-name
# pylint: disable=unused-argument


from pathlib import Path
from typing import Callable

import pytest

from pytest import MonkeyPatch

import hoppr.plugin_utils

from hoppr.base_plugins import collector
from hoppr.core_plugins.collect_nuget_plugin import CollectNugetPlugin, requests  # type: ignore[attr-defined]
from hoppr.models.credentials import CredentialRequiredService, Credentials
from hoppr.models.manifest import Repository
from hoppr.models.sbom import Component
from hoppr.models.types import PurlType
from hoppr.result import Result

test_component = Component(
    name="TestComponent",
    purl="pkg:nuget/example/package@1.2.3",
    type="file",  # type: ignore[arg-type]
)


@pytest.fixture(name="component")
def component_fixture() -> Component:
    """
    Test Component fixture
    """
    return test_component


@pytest.fixture
def plugin_fixture(plugin_fixture: CollectNugetPlugin, monkeypatch: MonkeyPatch, tmp_path: Path) -> CollectNugetPlugin:
    """
    Override and parametrize plugin_fixture to return CollectNugetPlugin
    """
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=lambda comp: ["https://somewhere1.com"])

    plugin_fixture.context.collect_root_dir = tmp_path
    plugin_fixture.context.repositories[PurlType.NUGET] = [
        Repository.parse_obj({"url": "https://somewhere2.com", "description": ""})
    ]

    return plugin_fixture


@pytest.mark.parametrize(
    argnames="response_fixture", argvalues=[{"status_code": 200, "content": test_component}], indirect=True
)
def test_collect_nuget(
    plugin_fixture: CollectNugetPlugin,
    find_credentials_fixture: CredentialRequiredService,
    response_fixture: Callable[[str], requests.Response],
    component: Component,
    monkeypatch: MonkeyPatch,
):
    """
    Test nuget collector successful run with a given url
    """
    write_bytes_called = False

    def mock_write_bytes(self, *args, **kwargs):  # pylint: disable=unused-argument
        nonlocal write_bytes_called
        write_bytes_called = True

    monkeypatch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
    monkeypatch.setattr(target=requests, name="get", value=response_fixture)
    monkeypatch.setattr(
        target=collector.Path, name="mkdir", value=lambda *args, **kwargs: None  # type: ignore[attr-defined]
    )
    monkeypatch.setattr(target=Path, name="write_bytes", value=mock_write_bytes)

    collect_result = plugin_fixture.process_component(component)

    assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}."
    assert write_bytes_called, "Package file was not saved."


RETRY_MESSAGE_TEMPLATE = "NuGet failed to locate package for {purl}, return_code={status_code}"
NOT_FOUND_FAILURE_MESSAGE_TEMPLATE = (
    f"{RETRY_MESSAGE_TEMPLATE}, "
    "URL should be full domain with path up to package, "
    "e.g. https://www.nuget.org/api/v2/package"
)


@pytest.mark.parametrize(
    argnames=["response_fixture", "expected_status_code", "expected_message_template"],
    argvalues=[
        pytest.param({"status_code": 401}, 401, RETRY_MESSAGE_TEMPLATE, id="Unauthorized (401)"),
        pytest.param({"status_code": 403}, 403, RETRY_MESSAGE_TEMPLATE, id="Forbidden (403)"),
        pytest.param({"status_code": 404}, 404, NOT_FOUND_FAILURE_MESSAGE_TEMPLATE, id="Not Found (404)"),
        pytest.param({"status_code": 500}, 500, RETRY_MESSAGE_TEMPLATE, id="Internal Server Error (500)"),
        pytest.param({"status_code": 502}, 502, RETRY_MESSAGE_TEMPLATE, id="Bad Gateway (502)"),
    ],
    indirect=["response_fixture"],
)
def test_collect_nuget_fail(  # pylint: disable=too-many-arguments
    plugin_fixture: CollectNugetPlugin,
    find_credentials_fixture: CredentialRequiredService,
    response_fixture: Callable[[str], requests.Response],
    component: Component,
    expected_message_template: str,
    expected_status_code: int,
    monkeypatch: MonkeyPatch,
):
    """
    Test nuget collector unsuccessful run with a given url
    """
    with monkeypatch.context() as patch:
        patch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
        patch.setattr(target=requests, name="get", value=response_fixture)
        patch.setattr(target=Path, name="mkdir", value=lambda *args, **kwargs: None)

        collect_result = plugin_fixture.process_component(component)

        assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
        assert collect_result.message.endswith(
            expected_message_template.format(purl=component.purl, status_code=expected_status_code)
        )
