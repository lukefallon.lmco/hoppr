"""
Test module for CollectPypiPlugin class
"""

# pylint: disable=redefined-outer-name

import importlib.util

from pathlib import Path
from subprocess import CompletedProcess
from typing import Iterator

import pytest

from hoppr_cyclonedx_models.cyclonedx_1_5 import Scope
from pytest import MonkeyPatch

import hoppr.plugin_utils

from hoppr.core_plugins.collect_pypi_plugin import CollectPypiPlugin
from hoppr.models.credentials import CredentialRequiredService, Credentials
from hoppr.models.manifest import Repository
from hoppr.models.sbom import Component
from hoppr.models.types import PurlType
from hoppr.result import Result


@pytest.fixture(name="component")
def component_fixture() -> Component:
    """
    Test Component fixture
    """
    return Component(name="TestComponent", purl="pkg:pypi/hoppr/hippo@1.2.3", type="file")  # type: ignore[arg-type]


@pytest.fixture(name="excluded_component")
def excluded_component_fixture() -> Component:
    """
    Test Component with Scope Excluded fixture
    """
    return Component(
        name="TestExcludedComponent",
        purl="pkg:pypi/something/not-needed@4.5.6",
        type="file",  # type: ignore[arg-type]
        scope=Scope.excluded,
    )


@pytest.fixture
def plugin_fixture(plugin_fixture: CollectPypiPlugin, monkeypatch: MonkeyPatch, tmp_path: Path) -> CollectPypiPlugin:
    """
    Override and parametrize plugin_fixture to return CollectPypiPlugin
    """
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(
        target=plugin_fixture, name="_get_repos", value=lambda comp: ["https://pypi.hoppr.com/hoppr/pypi"]
    )

    plugin_fixture.context.collect_root_dir = tmp_path
    plugin_fixture.context.repositories[PurlType.PYPI] = [
        Repository.parse_obj({"url": "https://somewhere.com", "description": ""})
    ]

    return plugin_fixture


@pytest.mark.usefixtures("config_fixture")
@pytest.mark.parametrize(
    argnames="config_fixture",
    argvalues=[
        {"type": "binary-only"},
        {"type": "binary-preferred"},
        {"type": "source-only"},
        {"type": "SOURCE-preferred"},
        {"type": "both-preferred"},
        {"type": "both-required"},
    ],
)
def test_collect_pypi_success(
    plugin_fixture: CollectPypiPlugin,
    run_command_fixture: CompletedProcess,
    component: Component,
    monkeypatch: MonkeyPatch,
):
    """
    Test collect method: success
    """
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)

    assert plugin_fixture.config
    collection_type = plugin_fixture.config["type"]

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_success(), f"Expected SUCCESS result for {collection_type}, got {collect_result}"


@pytest.mark.usefixtures("config_fixture")
@pytest.mark.parametrize(
    argnames=["config_fixture", "completed_process_generator"],
    argvalues=[
        ({"type": "binary-preferred"}, ["return_1", "return_0"]),
        ({"type": "SOURCE-preferred"}, ["return_1", "return_0"]),
        ({"type": "both-preferred"}, ["return_0", "return_1"]),
        ({"type": "both-preferred"}, ["return_1", "return_0"]),
    ],
    indirect=["completed_process_generator"],
)
def test_collect_pypi_success_2nd_try(
    plugin_fixture: CollectPypiPlugin,
    completed_process_generator: Iterator[CompletedProcess],
    component: Component,
    monkeypatch: MonkeyPatch,
):
    """
    Test collect method: success on second attempt
    """
    monkeypatch.setattr(
        target=plugin_fixture,
        name="run_command",
        value=lambda *_, **__: next(completed_process_generator),
    )

    assert plugin_fixture.config
    collection_type = plugin_fixture.config["type"]

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_success(), f"Expected SUCCESS result for {collection_type}, got {collect_result}"


def test_collect_pypi_success_different_pip(
    plugin_fixture: CollectPypiPlugin,
    run_command_fixture: CompletedProcess,
    component: Component,
    monkeypatch: MonkeyPatch,
):
    """
    Test collect method: success
    """
    config = {"pip_command": "python -m pip3"}
    # Disabling pylint for this line since we are trying to test something in the init method
    CollectPypiPlugin(context=plugin_fixture.context, config=config)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)
    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"


# Since failures result in re-tries, we need to supply return codes for up to six attempts,
# three each for the first and second collection


@pytest.mark.usefixtures("config_fixture")
@pytest.mark.parametrize(
    argnames=["config_fixture", "completed_process_generator", "expected_msg"],
    argvalues=[
        (
            {"type": "binary-only"},
            ["return_1", "return_1", "return_1", "return_0"],
            "Failure after 3 attempts, final message Failed to download hippo version 1.2.3. Unable to collect binary",
        ),
        (
            {"type": "binary-preferred"},
            ["return_1", "return_1", "return_1", "return_1", "return_1", "return_1"],
            "Failure after 3 attempts, final message Failed to download hippo version 1.2.3. "
            "Unable to download binary or source.",
        ),
        (
            {"type": "source-only"},
            ["return_1", "return_1", "return_1", "return_0"],
            "Failure after 3 attempts, final message Failed to download hippo version 1.2.3. Unable to collect source",
        ),
        (
            {"type": "SOURCE-preferred"},
            ["return_1", "return_1", "return_1", "return_1", "return_1", "return_1"],
            "Failure after 3 attempts, final message Failed to download hippo version 1.2.3. "
            "Unable to download source or binary.",
        ),
        (
            {"type": "both-preferred"},
            ["return_1", "return_1", "return_1", "return_1", "return_1", "return_1"],
            "Failure after 3 attempts, final message Failed to download hippo version 1.2.3. "
            "Unable to download binary or source.",
        ),
        (
            {"type": "both-required"},
            ["return_1", "return_1", "return_1", "return_0"],
            "Failure after 3 attempts, final message Failed to download hippo version 1.2.3. "
            "Unable to collect binary",
        ),
        (
            {"type": "both-required"},
            ["return_0", "return_1", "return_0", "return_1", "return_0", "return_1"],
            "Failure after 3 attempts, final message Failed to download hippo version 1.2.3. "
            "Only able to download binary",
        ),
        (
            {"type": "garbage"},
            ["return_0"],
            "Failed to download hippo version 1.2.3. Invalid pypi collection type specified: garbage",
        ),
    ],
    indirect=["config_fixture", "completed_process_generator"],
)
def test_collect_pypi_fail(  # pylint: disable=too-many-arguments
    expected_msg: str,
    plugin_fixture: CollectPypiPlugin,
    completed_process_generator: Iterator[CompletedProcess],
    find_credentials_fixture: CredentialRequiredService,
    component: Component,
    monkeypatch: MonkeyPatch,
):
    """
    Test collect method: fail
    """
    monkeypatch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
    monkeypatch.setattr(
        target=plugin_fixture,
        name="run_command",
        value=lambda *_, **__: next(completed_process_generator),
    )

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message == expected_msg


def test_collect_pypi_pip_not_found(plugin_fixture: CollectPypiPlugin, component: Component, monkeypatch: MonkeyPatch):
    """
    Test collect method: pip not found
    """
    monkeypatch.setattr(target=importlib.util, name="find_spec", value=lambda *args, **kwargs: None)

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_fail()
    assert collect_result.message == "The pip package was not found. Please install and try again."


def test_collector_excluded_pypi(
    plugin_fixture: CollectPypiPlugin,
    run_command_fixture: CompletedProcess,
    excluded_component: Component,
    monkeypatch: MonkeyPatch,
):
    """
    Test raw collector skip run given a purl with a scope of excluded
    """
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)
    collect_result = plugin_fixture.process_component(excluded_component)
    assert collect_result.is_excluded(), f"Expected EXCLUDED result, got {collect_result}"
