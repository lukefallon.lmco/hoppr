"""
Test module for ReportGenerator class
"""

# pylint: disable=protected-access

from pathlib import Path
import uuid

import pytest

from pytest import FixtureRequest, MonkeyPatch

from hoppr.core_plugins.report_generator import Report, ReportGenerator
from hoppr.models.sbom import Component
from hoppr.result import ResultStatus


@pytest.fixture(name="component")
def component_fixture() -> Component:
    """
    Test Component fixture
    """

    return Component(
        name="TestComponent",
        purl="pkg:docker/something/else@1.2.3",
        type="file",  # type: ignore[arg-type]
    )


@pytest.fixture(name="reports")
def reports_fixture(component: Component, request: FixtureRequest) -> list[Report]:
    """
    Test fixture to return list of Report objects
    """
    result_list = getattr(request, "param", [])

    return [
        Report(
            unique_id=uuid.uuid4(),
            plugin="CollectDockerPlugin",
            stage="Collect",
            result=result,
            details="",
            method="process_component",
            component=component,
        )
        for result in result_list
    ]


@pytest.mark.parametrize(
    argnames="reports",
    argvalues=[pytest.param([ResultStatus.SUCCESS, ResultStatus.FAIL], id="expected-files")],
    indirect=True,
)
def test_generate_report(
    plugin_fixture: ReportGenerator,
    reports: list[Report],
    monkeypatch: MonkeyPatch,
    tmp_path: Path,
):
    """
    Test generate_report method
    """
    monkeypatch.setattr(target=Path, name="cwd", value=lambda: tmp_path)

    plugin_fixture.report_gen_list = reports
    plugin_fixture.generate_report()

    assert (tmp_path / "report" / "index.html").is_file()
    assert (tmp_path / "report" / "assets").is_dir()
    assert (tmp_path / "report" / "scripts").is_dir()
    assert (tmp_path / "report" / "styles").is_dir()


@pytest.mark.parametrize(
    argnames=["reports", "expected"],
    argvalues=[
        ([ResultStatus.FAIL, ResultStatus.FAIL], 0),
        ([ResultStatus.SUCCESS, ResultStatus.FAIL], 1),
        ([ResultStatus.SUCCESS, ResultStatus.SUCCESS], 2),
    ],
    indirect=["reports"],
)
def test_get_successful_report_total(plugin_fixture: ReportGenerator, reports: list[Report], expected: int):
    """
    Test _get_successful_report_total method
    """
    assert plugin_fixture._get_successful_report_total(reports) == expected


@pytest.mark.parametrize(
    argnames=["reports", "expected"],
    argvalues=[
        ([ResultStatus.FAIL, ResultStatus.FAIL], "btn-danger"),
        ([ResultStatus.SUCCESS, ResultStatus.FAIL], "btn-warning"),
        ([ResultStatus.SUCCESS, ResultStatus.SUCCESS], "btn-success"),
    ],
    indirect=["reports"],
)
def test_get_overall_status_button_class(plugin_fixture: ReportGenerator, reports: list[Report], expected: str):
    """
    Test _get_overall_status_button_class method
    """
    assert plugin_fixture._get_overall_status_button_class(reports) == expected


@pytest.mark.parametrize(
    argnames=["sort_by", "expected"],
    argvalues=[("plugin", ["CollectMavenPlugin", "CollectAptPlugin", "TarBundle"]), ("stage", ["Collect", "Bundle"])],
)
def test_get_reports_by(plugin_fixture: ReportGenerator, component: Component, sort_by: str, expected: list):
    """
    Test _get_reports_by method
    """

    reports: list[Report] = [
        Report(uuid.uuid4(), "CollectMavenPlugin", "Collect", ResultStatus.SUCCESS, "", "process_component", component),
        Report(uuid.uuid4(), "CollectAptPlugin", "Collect", ResultStatus.FAIL, "", "process_component", component),
        Report(uuid.uuid4(), "TarBundle", "Bundle", ResultStatus.SUCCESS, "", "process_component"),
    ]

    sorted_reports = plugin_fixture._get_reports_by(sort_by, reports)

    assert list(sorted_reports.keys()) == expected
