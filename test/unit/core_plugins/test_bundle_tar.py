"""
Test module for TarBundlePlugin class
"""
import os
import tarfile

from pathlib import Path

from pytest import MonkeyPatch

from hoppr.core_plugins.bundle_tar import TarBundlePlugin


def test_defaults(plugin_fixture: TarBundlePlugin, tmp_path: Path, monkeypatch: MonkeyPatch):
    """
    Test TarBundlePlugin.post_stage_process method
    """
    with monkeypatch.context() as patch:
        patch.setattr(target=os.path, name="exists", value=lambda path: True)
        tar_file = tmp_path / "bundle.tar.gz"
        plugin_fixture.config = {"tarfile_name": str(tar_file)}
        result = plugin_fixture.post_stage_process()
        assert result.is_success(), f"Expected SUCCESS result, got {result}"


def test_no_comp_readerror(plugin_fixture: TarBundlePlugin, monkeypatch: MonkeyPatch):
    """
    Test TarBundlePlugin.post_stage_process method fails when tarfile.ReadError raised
    """

    def _mock_tarfile_open(*args, **kwargs):
        raise tarfile.ReadError

    monkeypatch.setattr(target=tarfile, name="open", value=_mock_tarfile_open)

    plugin_fixture.config = {"tarfile_name": "tar_file"}
    result = plugin_fixture.post_stage_process()
    assert result.is_fail(), f"Expected FAIL result, got {result}"
    assert len(plugin_fixture.get_version()) > 0


def test_xz_filenotfound(plugin_fixture: TarBundlePlugin, monkeypatch: MonkeyPatch):
    """
    Test TarBundlePlugin.post_stage_process method fails when FileNotFoundError raised
    """

    def _mock_tarfile_open(*args, **kwargs):
        raise FileNotFoundError

    monkeypatch.setattr(target=tarfile, name="open", value=_mock_tarfile_open)

    plugin_fixture.config = {"tarfile_name": "tar_file", "compression": "lzma"}
    result = plugin_fixture.post_stage_process()
    assert result.is_fail(), f"Expected FAIL result, got {result}"


def test_xz_permission(plugin_fixture: TarBundlePlugin, monkeypatch: MonkeyPatch):
    """
    Test TarBundlePlugin.post_stage_process method fails when PermissionError raised
    """

    def _mock_tarfile_open(*args, **kwargs):
        raise PermissionError

    monkeypatch.setattr(target=tarfile, name="open", value=_mock_tarfile_open)

    plugin_fixture.config = {"tarfile_name": "tar_file", "compression": "bz"}
    result = plugin_fixture.post_stage_process()
    assert result.is_fail(), f"Expected FAIL result, got {result}"


def test_bad_compression(plugin_fixture: TarBundlePlugin):
    """
    Test TarBundlePlugin.post_stage_process method fails with bad compression type
    """
    plugin_fixture.config = {"compression": "bad"}
    result = plugin_fixture.post_stage_process()
    assert result.is_fail(), f"Expected FAIL result, got {result}"


def test_fileexists_no_comp(plugin_fixture: TarBundlePlugin, tmp_path: Path):
    """
    Test TarBundlePlugin.post_stage_process method success - no compression
    """
    plugin_fixture.config = {"compression": "none", "tarfile_name": tmp_path / "my.tarfile.tar"}
    plugin_fixture.config["tarfile_name"].touch()
    result = plugin_fixture.post_stage_process()

    assert result.is_success(), f"Expected SUCCESS result, got {result}"


def test_fileexists_noext(plugin_fixture: TarBundlePlugin, tmp_path: Path):
    """
    Test TarBundlePlugin.post_stage_process method success - file exists
    """
    plugin_fixture.config = {"tarfile_name": tmp_path / "my.tarfile"}
    plugin_fixture.config["tarfile_name"].touch()
    result = plugin_fixture.post_stage_process()

    assert result.is_success(), f"Expected SUCCESS result, got {result}"
