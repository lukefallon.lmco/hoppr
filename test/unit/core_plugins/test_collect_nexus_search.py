"""
Unit tests for the Nexus Search Collector
"""

# pylint: disable=protected-access
# pylint: disable=redefined-outer-name

from pathlib import Path
from typing import Callable

import pytest
import requests

from pydantic import SecretStr
from pytest import FixtureRequest, MonkeyPatch
from requests.auth import HTTPBasicAuth

import hoppr.core_plugins.collect_nexus_search
import hoppr.utils

from hoppr.core_plugins.collect_nexus_search import CollectNexusSearch
from hoppr.models.credentials import CredentialRequiredService, Credentials
from hoppr.models.sbom import Component


@pytest.fixture(name="component")
def component_fixture(request: FixtureRequest):
    """
    Test Component fixture
    """
    purl_string = getattr(request, "param", "pkg:rpm/test_component@0.1.2")
    purl = hoppr.utils.get_package_url(purl_string)
    return Component.parse_obj({"name": purl.name, "version": purl.version, "purl": purl_string, "type": "file"})


@pytest.mark.parametrize(
    argnames=["config_fixture", "response_fixture"],
    argvalues=[
        ({"purl_types": ["rpm", "pip"]}, {"status_code": 200, "content": "mocked content"}),
    ],
    indirect=True,
)
def test_collect_nexus_success(
    plugin_fixture: CollectNexusSearch,
    find_credentials_fixture: CredentialRequiredService,
    response_fixture: Callable[[str], requests.Response],
    component: Component,
    monkeypatch: MonkeyPatch,
):
    """
    Test a successful call to collect_nexus
    """
    monkeypatch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=lambda comp: ["https://mock_nexus.com"])
    monkeypatch.setattr(target=CollectNexusSearch, name="is_nexus_instance", value=lambda *_, **__: True)
    monkeypatch.setattr(
        target=CollectNexusSearch,
        name="get_download_urls",
        value=lambda *_, **__: ["https://mock_nexus.com/repository/test/testobj.txt"],
    )
    monkeypatch.setattr(target=hoppr.core_plugins.collect_nexus_search, name="download_file", value=response_fixture)

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"


def test_collect_nexus_not_nexus(plugin_fixture: CollectNexusSearch, component: Component, monkeypatch: MonkeyPatch):
    """
    Test calling collect_nexus on a non-nexus repository
    """
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=lambda comp: ["https://mock_nexus.com"])
    monkeypatch.setattr(target=CollectNexusSearch, name="is_nexus_instance", value=lambda *_, **__: False)

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message == "https://mock_nexus.com is not a Nexus instance"


@pytest.mark.parametrize(argnames="component", argvalues=["pkg:git/something/else@1.2.3"], indirect=True)
def test_collect_nexus_git_not_supported(
    plugin_fixture: CollectNexusSearch, component: Component, monkeypatch: MonkeyPatch
):
    """
    Test calling collect_nexus for a git purl
    """
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=lambda comp: ["https://mock_nexus.com"])
    monkeypatch.setattr(target=CollectNexusSearch, name="is_nexus_instance", value=lambda *_, **__: True)

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_skip(), f"Expected SKIP result, got {collect_result}"


@pytest.mark.parametrize(
    argnames=["config_fixture", "response_fixture"],
    argvalues=[
        ({"purl_types": ["rpm", "pip"]}, {"status_code": 200, "content": "mocked content"}),
    ],
    indirect=True,
)
def test_collect_nexus_nothing_found(
    plugin_fixture: CollectNexusSearch,
    response_fixture: Callable[[str], requests.Response],
    component: Component,
    monkeypatch: MonkeyPatch,
):
    """
    Test calling collect_nexus with no artifact found
    """
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=lambda comp: ["https://mock_nexus.com"])
    monkeypatch.setattr(target=CollectNexusSearch, name="is_nexus_instance", value=lambda *_, **__: True)
    monkeypatch.setattr(target=CollectNexusSearch, name="get_download_urls", value=lambda *_, **__: [])
    monkeypatch.setattr(target=hoppr.core_plugins.collect_nexus_search, name="download_file", value=response_fixture)

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message == (
        "No artifacts found in Nexus instance https://mock_nexus.com for purl pkg:rpm/test_component@0.1.2"
    )


@pytest.mark.parametrize(
    argnames=["config_fixture", "response_fixture", "expected_result"],
    argvalues=[
        pytest.param(
            {"purl_types": ["rpm", "pip"]},
            {
                "status_code": 200,
                "content": b"""{
                    "items": [
                        {"downloadUrl": "http://my-nexus.com/repository/my-repo/1/test/file.ext"},
                        {"downloadUrl": "http://my-nexus.com/repository/my-repo/2/test/file.ext"},
                        {"downloadUrl": "http://my-nexus.com/repository/my-repo/3/file.ext"}
                    ]
                }
                """,
            },
            'More than one result found for "test_component" when searching "http://my-nexus.com". '
            "Please include a more specific repository URL in your manifest file. Either one of these should work:\n"
            "  \u2022\u00A0http://my-nexus.com/repository/my-repo/1\n"
            "  \u2022\u00A0http://my-nexus.com/repository/my-repo/2\n"
            "  \u2022\u00A0http://my-nexus.com/repository/my-repo/3\n",
            id="multiple-results",
        ),
        pytest.param(
            {"purl_types": ["rpm", "pip"]},
            {
                "status_code": 200,
                "content": b'{"items": []}',
            },
            "No artifacts found in Nexus instance http://my-nexus.com for purl pkg:rpm/test_component@0.1.2",
            id="no-results",
        ),
    ],
    indirect=["config_fixture", "response_fixture"],
)
def test_collect_nexus_ambiguous_download_urls(
    plugin_fixture: CollectNexusSearch,
    response_fixture: Callable[[str], requests.Response],
    component: Component,
    expected_result: str,
    monkeypatch: MonkeyPatch,
):
    """
    Test calling collect_nexus with a failure on download
    """
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=lambda comp: ["http://my-nexus.com"])
    monkeypatch.setattr(target=CollectNexusSearch, name="is_nexus_instance", value=lambda *_, **__: True)
    monkeypatch.setattr(target=requests, name="get", value=response_fixture)

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message == expected_result


@pytest.mark.parametrize(
    argnames=["config_fixture", "response_fixture"],
    argvalues=[
        ({"purl_types": ["rpm", "pip"]}, {"status_code": 404, "content": b"not found"}),
    ],
    indirect=True,
)
def test_collect_nexus_download_fail(
    plugin_fixture: CollectNexusSearch,
    response_fixture: Callable[[str], requests.Response],
    component: Component,
    monkeypatch: MonkeyPatch,
):
    """
    Test calling collect_nexus with a failure on download
    """
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=lambda comp: ["https://mock_nexus.com"])
    monkeypatch.setattr(target=CollectNexusSearch, name="is_nexus_instance", value=lambda *_, **__: True)
    monkeypatch.setattr(target=hoppr.core_plugins.collect_nexus_search, name="download_file", value=response_fixture)
    monkeypatch.setattr(
        target=CollectNexusSearch,
        name="get_download_urls",
        value=lambda *_, **__: ["https://mock_nexus.com/repository/test/testobj.txt"],
    )

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message == "HTTP Status Code: 404; not found"


@pytest.mark.parametrize(
    argnames=["purl_string", "expected_directory"],
    argvalues=[
        (
            "pkg:docker/namespace/image@1.2.3",
            Path("docker/https%3A%2F%2Fmy-nexus.com%2Frepository%2Fmy_repo/the_path"),
        ),
        (
            "pkg:pypi/namespace/module.py@1.2.3",
            Path("pypi/https%3A%2F%2Fmy-nexus.com%2Frepository%2Fmy_repo/module.py_1.2.3"),
        ),
    ],
)
def test_directory_for_nexus(plugin_fixture: CollectNexusSearch, purl_string: str, expected_directory: Path):
    """
    Test the directory_for_nexus method
    """
    repo_url = "https://my-nexus.com/repository/my_repo/the_path/file.txt"

    purl = hoppr.utils.get_package_url(purl_string)
    assert plugin_fixture._directory_for_nexus(purl, repo_url) == (
        plugin_fixture.context.collect_root_dir / expected_directory
    )


@pytest.mark.parametrize(argnames="response_generator", argvalues=[["response_500", "response_200"]], indirect=True)
def test_is_nexus(
    cred_object_fixture: CredentialRequiredService,
    response_generator: Callable[..., requests.Response],
    monkeypatch: MonkeyPatch,
):
    """
    Test the is_nexus method on success
    """
    monkeypatch.setattr(target=requests, name="get", value=response_generator)

    auth: HTTPBasicAuth | None = None
    if cred_object_fixture is not None and isinstance(cred_object_fixture.password, SecretStr):
        auth = HTTPBasicAuth(
            username=cred_object_fixture.username, password=cred_object_fixture.password.get_secret_value()
        )

    assert CollectNexusSearch.is_nexus_instance("http://my-nexus.com", auth)


@pytest.mark.parametrize(
    argnames="response_fixture", argvalues=[{"status_code": 404, "content": b"not found"}], indirect=True
)
def test_is_not_nexus(
    response_fixture: Callable[[str], requests.Response],
    cred_object_fixture: CredentialRequiredService,
    monkeypatch: MonkeyPatch,
):
    """
    Test the is_nexus method on failure
    """
    monkeypatch.setattr(target=requests, name="get", value=response_fixture)

    auth: HTTPBasicAuth | None = None
    if cred_object_fixture is not None and isinstance(cred_object_fixture.password, SecretStr):
        auth = HTTPBasicAuth(
            username=cred_object_fixture.username, password=cred_object_fixture.password.get_secret_value()
        )

    assert not CollectNexusSearch.is_nexus_instance("http://my-nexus.com", auth)


@pytest.mark.parametrize(
    argnames="response_fixture", argvalues=[{"status_code": 500, "content": b"network issue"}], indirect=True
)
def test_is_nexus_multifail(
    response_fixture: Callable[[str], requests.Response],
    cred_object_fixture: CredentialRequiredService,
    monkeypatch: MonkeyPatch,
):
    """
    Test the is_nexus method with network failures
    """
    monkeypatch.setattr(target=requests, name="get", value=response_fixture)

    auth: HTTPBasicAuth | None = None
    if cred_object_fixture is not None and isinstance(cred_object_fixture.password, SecretStr):
        auth = HTTPBasicAuth(
            username=cred_object_fixture.username, password=cred_object_fixture.password.get_secret_value()
        )

    assert not CollectNexusSearch.is_nexus_instance("http://my-nexus.com", auth)


@pytest.mark.parametrize(
    argnames="response_fixture",
    argvalues=[
        {
            "status_code": 200,
            "content": b'{"items": [{ "downloadUrl": "http://my-nexus.com/repository/my-repo/file.ext"}]}',
        }
    ],
    indirect=True,
)
@pytest.mark.parametrize(
    argnames=["purl_string", "expected_urls"],
    argvalues=[
        ("pkg:deb/namespace/file.txt@1.2.3", ["http://my-nexus.com/repository/my-repo/file.ext"]),
        ("pkg:gem/namespace/file.txt@1.2.3", ["http://my-nexus.com/repository/my-repo/file.ext"]),
        ("pkg:golang/namespace/file.txt@1.2.3", ["http://my-nexus.com/repository/my-repo/file.ext"]),
        ("pkg:generic/namespace/file.txt@1.2.3", ["http://my-nexus.com/repository/my-repo/file.ext"]),
        ("pkg:raw/namespace/file.txt@1.2.3", ["http://my-nexus.com/repository/my-repo/file.ext"]),
        ("pkg:rpm/namespace/file.txt@1.2.3", ["http://my-nexus.com/repository/my-repo/file.ext"]),
        ("pkg:npm/namespace/@semantic-release@1.2.3", ["http://my-nexus.com/repository/my-repo/file.ext"]),
        (
            "pkg:maven/namespace/file.txt@1.2.3",
            [
                "http://my-nexus.com/repository/my-repo/file.ext",
                "http://my-nexus.com/repository/my-repo/file.ext",
                "http://my-nexus.com/repository/my-repo/file.ext",
            ],
        ),
    ],
)
def test_get_download_urls(
    plugin_fixture: CollectNexusSearch,
    purl_string: str,
    expected_urls: list[str],
    response_fixture: Callable[[str], requests.Response],
    monkeypatch: MonkeyPatch,
):
    """
    Test the get_download_urls method
    """
    monkeypatch.setattr(target=requests, name="get", value=response_fixture)

    purl = hoppr.utils.get_package_url(purl_string)
    assert plugin_fixture.get_download_urls(purl, "http://my-nexus.com/repository/my-repo") == expected_urls


@pytest.mark.parametrize(
    argnames="response_fixture",
    argvalues=[
        {
            "status_code": 200,
            "content": b'{"items": [{ "downloadUrl": "http://my-nexus.com/repository/my-repo/1/test/file.ext"},'
            b'{ "downloadUrl": "http://my-nexus.com/repository/my-repo/2/test/file.ext"},'
            b'{ "downloadUrl": "http://my-nexus.com/repository/my-repo/3/file.ext"}]}',
        },
        {
            "status_code": 200,
            "content": b'{"items": [{ "downloadUrl": "http://my-nexus.com/repository/my-repo/1/test/file.ext"},'
            b'{ "downloadUrl": "http://my-nexus.com/repository/my-repo/2/test/file.ext"},'
            b'{ "downloadUrl": "http://my-nexus.com/repository/my-repo/2/file.ext"}]}',
        },
        {
            "status_code": 200,
            "content": b'{"items": [{ "downloadUrl": "http://my-nexus.com/repository/my-repo/1/test/file.ext"},'
            b'{ "downloadUrl": "http://my-nexus.com/repository/my-repo/1/test/file.ext"}]}',
        },
    ],
    indirect=True,
)
@pytest.mark.parametrize(
    argnames="purl_string",
    argvalues=[
        "pkg:rpm/namespace/file.txt@1.2.3",
    ],
)
def test_get_download_urls_ambiguity_errors(
    plugin_fixture: CollectNexusSearch,
    purl_string: str,
    response_fixture: Callable[[str], requests.Response],
    monkeypatch: MonkeyPatch,
):
    """
    Test the get_download_urls method with ambiguous results.
    """
    monkeypatch.setattr(target=requests, name="get", value=response_fixture)

    purl = hoppr.utils.get_package_url(purl_string)
    with pytest.raises(ValueError):
        plugin_fixture.get_download_urls(purl, "http://my-nexus.com/repository/my-repo")


@pytest.mark.parametrize(
    argnames="response_fixture",
    argvalues=[
        {
            "status_code": 200,
            "content": b'{"items": [{ "downloadUrl": "https://my-nexus.com/repository/my-repo/file.ext"}]}',
        }
    ],
    indirect=True,
)
@pytest.mark.parametrize(
    argnames=["purl_string", "expected_urls"],
    argvalues=[
        ("pkg:deb/namespace/file.txt@1.2.3", ["https://my-nexus.com/repository/my-repo/file.ext"]),
        ("pkg:gem/namespace/file.txt@1.2.3", ["https://my-nexus.com/repository/my-repo/file.ext"]),
        ("pkg:golang/namespace/file.txt@1.2.3", ["https://my-nexus.com/repository/my-repo/file.ext"]),
        ("pkg:generic/namespace/file.txt@1.2.3", ["https://my-nexus.com/repository/my-repo/file.ext"]),
        ("pkg:raw/namespace/file.txt@1.2.3", ["https://my-nexus.com/repository/my-repo/file.ext"]),
        ("pkg:rpm/namespace/file.txt@1.2.3", ["https://my-nexus.com/repository/my-repo/file.ext"]),
        ("pkg:npm/@semantic-release/npm@1.2.3", ["https://my-nexus.com/repository/my-repo/file.ext"]),
        (
            "pkg:maven/namespace/file.txt@1.2.3",
            [
                "https://my-nexus.com/repository/my-repo/file.ext",
                "https://my-nexus.com/repository/my-repo/file.ext",
                "https://my-nexus.com/repository/my-repo/file.ext",
            ],
        ),
    ],
)
def test_get_download_urls_https(
    plugin_fixture: CollectNexusSearch,
    purl_string: str,
    expected_urls: list[str],
    response_fixture: Callable[[str], requests.Response],
    monkeypatch: MonkeyPatch,
):
    """
    Test the get_download_urls method
    """
    monkeypatch.setattr(target=requests, name="get", value=response_fixture)

    purl = hoppr.utils.get_package_url(purl_string)
    assert plugin_fixture.get_download_urls(purl, "https://my-nexus.com/repository/my-repo") == expected_urls


@pytest.mark.parametrize(
    argnames="response_fixture",
    argvalues=[{"status_code": 500, "content": b"Internal Server Error (500)"}],
    indirect=True,
)
def test_get_download_urls_bad_response(
    plugin_fixture: CollectNexusSearch,
    response_fixture: Callable[[str], requests.Response],
    monkeypatch: MonkeyPatch,
):
    """
    Test the get_download_urls method
    """
    monkeypatch.setattr(target=requests, name="get", value=response_fixture)

    purl = hoppr.utils.get_package_url("pkg:deb/namespace/file.txt@1.2.3")
    assert not plugin_fixture.get_download_urls(purl, "https://my-nexus.com/repository/my-repo")


def test_get_attestation_prods_config():
    """
    Test getting attestation products as configured by the user
    """
    config = {"purl_types": ["alpha", "bravo"]}

    prods = CollectNexusSearch.get_attestation_products(config)

    assert prods == ["alpha/*", "bravo/*"]


def test_get_attestation_prods_no_config():
    """
    Test getting default attestation products
    """
    prods = CollectNexusSearch.get_attestation_products()

    assert prods == [
        "cargo/*",
        "deb/*",
        "docker/*",
        "gem/*",
        "generic/*",
        "golang/*",
        "helm/*",
        "maven/*",
        "npm/*",
        "nuget/*",
        "oci/*",
        "pypi/*",
        "raw/*",
        "repo/*",
        "rpm/*",
    ]
