"""
Test module for CollectDnfPlugin class
"""

# pylint: disable=redefined-outer-name
# pylint: disable=unused-argument

from __future__ import annotations

import os
import shutil

from configparser import ConfigParser
from pathlib import Path
from subprocess import CompletedProcess
from typing import TYPE_CHECKING, Callable, Generator, Optional

import psutil
import pytest
import requests

from pytest import MonkeyPatch

import hoppr.core_plugins.collect_dnf_plugin
import hoppr.net
import hoppr.plugin_utils

from hoppr.core_plugins.collect_dnf_plugin import CollectDnfPlugin, _clear_cache, _is_rpm_repo, _repo_proxy
from hoppr.models.credentials import CredentialRequiredService, Credentials
from hoppr.models.manifest import Repository
from hoppr.models.sbom import Component
from hoppr.models.types import PurlType
from hoppr.result import Result

if TYPE_CHECKING:
    from packageurl import PackageURL


def mock__get_component_download_url(purl: PackageURL) -> str:
    """
    Mock the _get_download_url_path method
    """
    return "https://rpm.hoppr.com/dl/8/AppStream/x86_64/os/Packages/h/hippo-1.0.0-1.el8.noarch.rpm"


def mock__get_download_url_path(purl: PackageURL) -> str:
    """
    Mock the _get_download_url_path method
    """
    return "dl/8/AppStream/x86_64/os/Packages/h/hippo-1.0.0-1.el8.noarch.rpm"


def mock__get_found_repo(found_url: str) -> str:
    """
    Mock the _get_found_repo method
    """
    return "https://rpm.hoppr.com"


@pytest.fixture(name="component")
def component_fixture():
    """
    Test Component fixture
    """
    return Component(name="TestComponent", purl="pkg:rpm/hippo@1.0.0-1.el8?arch=noarch", type="file")


@pytest.fixture
def plugin_fixture(
    plugin_fixture: CollectDnfPlugin, config_fixture: dict[str, str], tmp_path: Path
) -> CollectDnfPlugin:
    """
    Override and parametrize plugin_fixture to return CollectDnfPlugin
    """
    plugin_fixture.context.collect_root_dir = tmp_path
    plugin_fixture.config_file = tmp_path / ".hoppr-dnf" / "dnf.conf"
    plugin_fixture.context.repositories[PurlType.RPM] = [
        Repository.parse_obj({"url": "https://somewhere.com", "description": ""})
    ]

    return plugin_fixture


@pytest.mark.parametrize(
    argnames=["found_url", "expected"],
    argvalues=[
        pytest.param(
            "https://rpm.hoppr.com/dl/8/AppStream/x86_64/os/Packages/h/hippo-1.0.0-1.el8.noarch.rpm",
            "https://rpm.hoppr.com/dl/8/AppStream/x86_64/os",
            id="repo_found",
        ),
        pytest.param(
            "https://somewhere.else.com/hippo-1.0.0-1.el8.noarch.rpm",
            None,
            id="repo_not_found",
        ),
    ],
)
def test__get_found_repo(
    plugin_fixture: CollectDnfPlugin, monkeypatch: MonkeyPatch, found_url: str, expected: Optional[str]
):
    """
    Test _get_found_repo method:
    """
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)

    result = plugin_fixture._get_found_repo(found_url)  # pylint: disable=protected-access
    assert result == expected


@pytest.mark.parametrize(
    argnames=["response_fixture", "expected_result"],
    argvalues=[
        pytest.param({"status_code": 200}, True, id="OK (200)"),
        pytest.param({"status_code": 404}, False, id="Not Found (404)"),
        pytest.param({"status_code": 500}, False, id="Internal Server Error (500)"),
    ],
    indirect=["response_fixture"],
)
def test__is_rpm_repo(
    expected_result: bool,
    find_credentials_fixture: CredentialRequiredService,
    response_fixture: Callable[[str], requests.Response],
    monkeypatch: MonkeyPatch,
):
    """
    Test _is_rpm_repo function
    """
    monkeypatch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
    monkeypatch.setattr(target=requests, name="get", value=response_fixture)

    result = _is_rpm_repo("http://my.host.com")
    assert result is expected_result


def test__repo_proxy(monkeypatch: MonkeyPatch):
    """
    Test _repo_proxy method
    """
    monkeypatch.setattr(
        target=os,
        name="getenv",
        value=lambda *x: {
            ("https_proxy", "_none_"): "https://proxy.somewhere.com",
            ("no_proxy", ""): "somewhere.else.com",
        }[x],
    )

    repo_url = "https://mirror.somewhere.else.com/rpmtest-1.0.0-0.el8.x86_64.rpm"
    result = _repo_proxy(repo_url)
    assert result == "_none_"


def test_collect_dnf_success(plugin_fixture: CollectDnfPlugin, monkeypatch: MonkeyPatch, component: Component):
    """
    Test collect method: success
    """
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(
        target=plugin_fixture, name="_get_component_download_url", value=mock__get_component_download_url
    )
    monkeypatch.setattr(target=plugin_fixture, name="_get_found_repo", value=mock__get_found_repo)
    monkeypatch.setattr(target=plugin_fixture, name="check_purl_specified_url", value=Result.success)
    monkeypatch.setattr(target=hoppr.net, name="download_file", value=lambda url, dest, creds: None)
    monkeypatch.setattr(target=Result, name="from_http_response", value=Result.success)

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"


@pytest.mark.parametrize(argnames="completed_process_fixture", argvalues=[{"returncode": 1}], indirect=True)
def test_collect_dnf_fail(
    plugin_fixture: CollectDnfPlugin,
    run_command_fixture: CompletedProcess,
    find_credentials_fixture: CredentialRequiredService,
    monkeypatch: MonkeyPatch,
    component: Component,
):
    """
    Test collect method: fail
    """
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)
    monkeypatch.setattr(target=Credentials, name="find", value=find_credentials_fixture)

    collect_result = plugin_fixture.process_component(component)

    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message == (
        f"Failure after 3 attempts, final message dnf failed to locate package for {component.purl}"
    )


@pytest.mark.parametrize(argnames="config_fixture", argvalues=[{"dnf_command": "dnf"}], indirect=True)
def test_collect_dnf_command_not_found(
    plugin_fixture: CollectDnfPlugin, monkeypatch: MonkeyPatch, component: Component
):
    """
    Test collect method: `dnf` command not found in PATH
    """
    # pylint: disable=duplicate-code
    monkeypatch.setattr(
        target=hoppr.plugin_utils,
        name="check_for_missing_commands",
        value=lambda message: Result.fail("[mock] command not found"),
    )

    collect_result = plugin_fixture.process_component(component)

    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message == "[mock] command not found"


def test_collect_dnf_fail_download(plugin_fixture: CollectDnfPlugin, monkeypatch: MonkeyPatch, component: Component):
    """
    Test collect method: download component fail
    """
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(
        target=plugin_fixture,
        name="_get_component_download_url",
        value=mock__get_component_download_url,
    )
    monkeypatch.setattr(target=plugin_fixture, name="_get_found_repo", value=mock__get_found_repo)
    monkeypatch.setattr(target=plugin_fixture, name="check_purl_specified_url", value=Result.success)
    monkeypatch.setattr(target=hoppr.net, name="download_file", value=lambda url, dest, creds: None)
    monkeypatch.setattr(target=Result, name="from_http_response", value=Result.retry)

    collect_result = plugin_fixture.process_component(component)

    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message.startswith("Failure after 3 attempts, final message Failed to download DNF artifact")


def test_collect_dnf_repo_not_found(plugin_fixture: CollectDnfPlugin, monkeypatch: MonkeyPatch, component: Component):
    """
    Test collect method: RPM file base URL not found in manifest repos
    """
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(
        target=plugin_fixture, name="_get_component_download_url", value=mock__get_component_download_url
    )
    monkeypatch.setattr(target=plugin_fixture, name="_get_found_repo", value=lambda found_url: None)

    collect_result = plugin_fixture.process_component(component)

    assert collect_result.is_fail()
    assert collect_result.message == (
        "Successfully found RPM file but URL does not match any repository in manifest. "
        "(Found URL: 'https://rpm.hoppr.com/dl/8/AppStream/x86_64/os/Packages/h/hippo-1.0.0-1.el8.noarch.rpm')"
    )


@pytest.mark.parametrize(
    argnames="completed_process_fixture",
    argvalues=[
        {
            "returncode": 0,
            "stdout": b"https://rpm.hoppr.com/dl/8/AppStream/x86_64/os/Packages/h/hippo-1.0.0-1.el8.noarch.rpm",
        }
    ],
    indirect=True,
)
def test_collect_dnf_url_mismatch(
    plugin_fixture: CollectDnfPlugin,
    run_command_fixture: CompletedProcess,
    monkeypatch: MonkeyPatch,
    component: Component,
):
    """
    Test collect method: `repository_url` qualifer not found in manifest repos
    """
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_found_repo", value=lambda found_url: "some.other.repo")
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)

    with monkeypatch.context() as patch:
        patch.setattr(
            target=component,
            name="purl",
            value="pkg:rpm/hippo@1.0.0-1.el8?arch=noarch&repository_url=https://rpm.hoppr.com",
        )

        collect_result = plugin_fixture.process_component(component)

    assert collect_result.is_fail()
    assert collect_result.message == (
        "Purl-specified repository url (https://rpm.hoppr.com) does not match current repo (some.other.repo)."
    )


def test_pre_stage_process_success(
    plugin_fixture: CollectDnfPlugin,
    run_command_fixture: CompletedProcess,
    find_credentials_fixture: CredentialRequiredService,
    monkeypatch: MonkeyPatch,
):
    """
    Test pre_stage_process method: success
    """

    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)
    monkeypatch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
    monkeypatch.setattr(target=hoppr.core_plugins.collect_dnf_plugin, name="_is_rpm_repo", value=lambda repo_url: True)

    pre_stage_process_result = plugin_fixture.pre_stage_process()
    assert pre_stage_process_result.is_success()


@pytest.mark.parametrize(argnames="completed_process_fixture", argvalues=[{"returncode": 1}], indirect=True)
def test_pre_stage_process_fail_cache(
    plugin_fixture: CollectDnfPlugin, run_command_fixture: CompletedProcess, monkeypatch: MonkeyPatch
):
    """
    Test pre_stage_process method: dnf makecache fail
    """
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)
    monkeypatch.setattr(target=hoppr.core_plugins.collect_dnf_plugin, name="_is_rpm_repo", value=lambda repo_url: True)
    monkeypatch.setattr(
        target=psutil,
        name="disk_partitions",
        value=lambda all: [
            psutil._common.sdiskpart(  # pylint: disable=protected-access
                device="vagrant",
                mountpoint="/",
                fstype="vboxsf",
                opts="rw,nodev,relatime,iocharset=utf8,uid=1001,gid=1001",
                maxfile=255,
                maxpath=4096,
            )
        ],
    )

    pre_stage_process_result = plugin_fixture.pre_stage_process()

    assert pre_stage_process_result.is_fail()
    assert pre_stage_process_result.message == "Failed to populate DNF cache."


def test_pre_stage_process_no_strict(
    plugin_fixture: CollectDnfPlugin,
    find_credentials_fixture: CredentialRequiredService,
    run_command_fixture: CompletedProcess,
    monkeypatch: MonkeyPatch,
    tmp_path: Path,
):
    """
    Test pre_stage_process method: `--no-strict`
    """

    def mock_path_glob(self: Path, pattern: str) -> Generator[Path, None, None]:
        yield tmp_path / "hoppr-test.repo"

    def mock_config_parser_read(self: ConfigParser, filenames: list[str]) -> list[str]:
        self.add_section(section="system-repo-1")
        self.add_section(section="system-repo-2")
        self["system-repo-1"] = {"enabled": "1"}
        self["system-repo-2"] = {"enabled": "0"}

        return [str(tmp_path / "hoppr-test.repo")]

    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)
    monkeypatch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
    monkeypatch.setattr(target=Path, name="glob", value=mock_path_glob)
    monkeypatch.setattr(target=ConfigParser, name="read", value=mock_config_parser_read)
    monkeypatch.setattr(target=hoppr.core_plugins.collect_dnf_plugin, name="_is_rpm_repo", value=lambda repo_url: True)

    plugin_fixture.context.strict_repos = False
    pre_stage_process_result = plugin_fixture.pre_stage_process()
    assert pre_stage_process_result.is_success()


def test_pre_stage_process_write_file_fail(plugin_fixture: CollectDnfPlugin, monkeypatch: MonkeyPatch, tmp_path: Path):
    """
    Test pre_stage_process method: attempt to write `dnf.conf` fail
    """
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=hoppr.core_plugins.collect_dnf_plugin, name="_is_rpm_repo", value=lambda repo_url: False)

    def mock_path_open(self: Path, mode: str, encoding: str):
        raise OSError("Mock I/O error")

    monkeypatch.setattr(target=Path, name="open", value=mock_path_open)

    plugin_fixture.config_file = tmp_path / ".hoppr-dnf" / "dnf.conf"

    pre_stage_process_result = plugin_fixture.pre_stage_process()
    assert pre_stage_process_result.is_fail(), f"Expected FAIL result, got {pre_stage_process_result}"
    assert pre_stage_process_result.message.startswith("Unable to write DNF repository config file: Mock I/O error")


def test_post_stage_process_success(plugin_fixture: CollectDnfPlugin, monkeypatch: MonkeyPatch, tmp_path: Path):
    """
    Test post_stage_process method: success
    """
    mock_copytree = (
        tmp_path / ".hoppr-dnf" / "rpm" / "https%3A%2F%2Fsomewhere.com%2F%24releasever%2F%24basearch" / "repodata"
    )

    def mock_path_exists(*args, **kwargs) -> bool:
        return True

    def mock_path_unlink(*args, **kwargs):
        return None

    def mock_path_rglob(pattern: str, *args, **kwargs) -> Generator[Path, None, None]:
        yield tmp_path / "hoppr-tmp-0" / "repodata"

    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=Path, name="unlink", value=mock_path_unlink)
    monkeypatch.setattr(target=Path, name="exists", value=mock_path_exists)
    monkeypatch.setattr(target=Path, name="rglob", value=mock_path_rglob)
    monkeypatch.setattr(target=shutil, name="copytree", value=lambda src, dst, dirs_exist_ok: mock_copytree)

    plugin_fixture.config_file.parent.mkdir(parents=True, exist_ok=True)
    plugin_fixture.config_file.write_text("[hoppr-tmp-0]\nbaseurl=https://somewhere.com/$releasever/$basearch\n")

    post_stage_process_result = plugin_fixture.post_stage_process()
    assert post_stage_process_result.is_success()


def test_post_stage_process_rm_file_fail(plugin_fixture: CollectDnfPlugin, monkeypatch: MonkeyPatch, tmp_path: Path):
    """
    Test post_stage_process method: attempt deleting `dnf.conf` fail
    """
    mock_copytree = (
        tmp_path / ".hoppr-dnf" / "rpm" / "https%3A%2F%2Fsomewhere.com%2F%24releasever%2F%24basearch" / "repodata"
    )

    def mock_path_exists(*args, **kwargs) -> bool:
        return True

    def mock_path_unlink(*args, **kwargs):
        raise FileNotFoundError

    def mock_path_rglob(pattern: str, *args, **kwargs) -> Generator[Path, None, None]:
        yield tmp_path / "hoppr-tmp-0" / "repodata"

    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=Path, name="unlink", value=mock_path_unlink)
    monkeypatch.setattr(target=Path, name="exists", value=mock_path_exists)
    monkeypatch.setattr(target=Path, name="rglob", value=mock_path_rglob)
    monkeypatch.setattr(target=shutil, name="copytree", value=lambda src, dst, dirs_exist_ok: mock_copytree)

    plugin_fixture.config_file.parent.mkdir(parents=True, exist_ok=True)
    plugin_fixture.config_file.write_text("[hoppr-tmp-0]\nbaseurl=https://somewhere.com/$releasever/$basearch\n")

    post_stage_process_result = plugin_fixture.post_stage_process()
    assert post_stage_process_result.is_fail()


def test_clear_cache():
    """
    Test for when clearing cache, all folders and files are removed in the tmp path
    """
    test_path = os.path.join("tmp_path", "hoppr-tmp-0", "repo_data")
    os.makedirs(test_path)
    assert os.path.exists(test_path) is True
    _clear_cache(Path("tmp_path"))
    assert os.path.exists(test_path) is False
