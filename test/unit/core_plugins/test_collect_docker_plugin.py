"""
Test module for CollectDockerPlugin class
"""

import logging
import re

from os import PathLike
from pathlib import Path
from subprocess import CompletedProcess

import pytest

from pytest import FixtureRequest, MonkeyPatch

import hoppr.plugin_utils
import hoppr.utils

from hoppr.core_plugins.collect_docker_plugin import CollectDockerPlugin
from hoppr.models.credentials import CredentialRequiredService, Credentials
from hoppr.models.sbom import Component
from hoppr.result import Result


@pytest.fixture(name="component")
def component_fixture(request: FixtureRequest) -> Component:
    """
    Test Component fixture
    """
    param_dict = dict(getattr(request, "param", {}))

    param_dict["name"] = param_dict.get("name", "test-component")
    param_dict["purl"] = param_dict.get("purl", "pkg:docker/something/else@1.2.3")
    param_dict["type"] = param_dict.get("type", "file")

    return Component(**param_dict)


def _mock_get_repos(comp: Component) -> list[str]:  # pylint: disable=unused-argument
    """
    Mock _get_repos method
    """

    return ["http://somewhere.com", "https://somewhere.com"]


@pytest.mark.parametrize(
    argnames=["completed_process_fixture", "component", "source_image", "directory_string"],
    argvalues=[
        pytest.param(
            {"returncode": 0},
            {"purl": "pkg:docker/something/else@1.2.3"},
            "docker://somewhere.com/something/else:1.2.3",
            "somewhere.com/something/else@1.2.3",
            id="semantic",
        ),
        pytest.param(
            {"returncode": 0},
            {
                "purl": "pkg:docker/something/else@sha256%3Aca5534a51dd04bbcebe9b23ba05f389466cf0c190f1f8f182d7eea92a9671d00"  # pylint: disable=line-too-long
            },
            "docker://somewhere.com/something/else@sha256:ca5534a51dd04bbcebe9b23ba05f389466cf0c190f1f8f182d7eea92a9671d00",  # pylint: disable=line-too-long
            "somewhere.com/something/else@sha256%3Aca5534a51dd04bbcebe9b23ba05f389466cf0c190f1f8f182d7eea92a9671d00",
            id="sha256",
        ),
        pytest.param(
            {"returncode": 0},
            {"purl": "pkg:docker/something/else@40d8a1af82bcc8472cf251c7c47e7b217379801704f99c6c6dac3a30e036f4d1"},
            "docker://somewhere.com/something/else@sha256:40d8a1af82bcc8472cf251c7c47e7b217379801704f99c6c6dac3a30e036f4d1",  # pylint: disable=line-too-long
            "somewhere.com/something/else@sha256%3A40d8a1af82bcc8472cf251c7c47e7b217379801704f99c6c6dac3a30e036f4d1",
            id="hash",
        ),
        pytest.param(
            {"returncode": 0},
            {"purl": "pkg:docker/something/else@latest?repository_url=docker.io"},
            "docker://somewhere.com/something/else:latest",
            "somewhere.com/something/else@latest",
            id="qualifier",
        ),
    ],
    indirect=["completed_process_fixture", "component"],
)
def test_collect_docker_success(  # pylint: disable=too-many-arguments
    plugin_fixture: CollectDockerPlugin,
    component: Component,
    source_image: str,
    directory_string: str,
    completed_process_fixture: CompletedProcess,
    monkeypatch: MonkeyPatch,
):
    """
    Test a successful run of the Docker Collector
    """
    already_run = False

    def _run_command(
        command: list[str],
        password_list: list[str] | None = None,
        cwd: str | PathLike[str] | None = None,
    ) -> CompletedProcess[bytes]:
        nonlocal already_run
        if already_run:
            pytest.fail(reason="expected `run_command` to be called exactly once")

        already_run = True

        assert command == [
            "skopeo",
            "copy",
            "--src-tls-verify=false",
            "--debug",
            source_image,
            f"docker-archive:{plugin_fixture.context.collect_root_dir}/docker/http%3A%2F%2F{directory_string}",
        ]
        assert password_list == []
        assert cwd is None

        return completed_process_fixture

    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=_mock_get_repos)

    plugin_fixture.get_logger().setLevel(logging.DEBUG)

    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=_run_command)

    collect_result = plugin_fixture.process_component(component)

    assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"

    # Validate that url pattern matches regex.
    # Regex docker://word
    pattern = re.compile(r"^docker://\w+.*")
    purl = hoppr.utils.get_package_url(component.purl)
    src_image = plugin_fixture._get_image(url="https://docker.io", purl=purl)  # pylint: disable=protected-access
    matched = bool(pattern.match(src_image.url))
    assert matched


@pytest.mark.parametrize(argnames="completed_process_fixture", argvalues=[{"returncode": 1}], indirect=True)
def test_collect_docker_fail(
    plugin_fixture: CollectDockerPlugin,
    monkeypatch: MonkeyPatch,
    component: Component,
    run_command_fixture: CompletedProcess,
    find_credentials_fixture: CredentialRequiredService,
):
    """
    Test a failing run of the Docker Collector
    """

    monkeypatch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=_mock_get_repos)
    monkeypatch.setattr(target=Path, name="exists", value=lambda *_, **__: True)
    monkeypatch.setattr(target=Path, name="unlink", value=lambda *_, **__: None)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message.startswith(
        "Failure after 3 attempts, final message Skopeo failed to copy docker image"
    )


@pytest.mark.parametrize(argnames="config_fixture", argvalues=[{"skopeo_command": "skopeo"}], indirect=True)
def test_collect_docker_command_not_found(
    plugin_fixture: CollectDockerPlugin,
    monkeypatch: MonkeyPatch,
    component: Component,
    run_command_fixture: CompletedProcess,
):
    """
    Test if the required command is not found
    """
    # pylint: disable=duplicate-code
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=_mock_get_repos)
    monkeypatch.setattr(
        target=hoppr.plugin_utils,
        name="check_for_missing_commands",
        value=lambda message: Result.fail("[mock] command not found"),
    )

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message == "[mock] command not found"


@pytest.fixture(name="oci_component")
def oci_component_fixture(request: FixtureRequest) -> Component:
    """
    Test Oci Component fixture
    """
    param_dict = dict(getattr(request, "param", {}))

    param_dict["name"] = param_dict.get("name", "test-component")
    param_dict["purl"] = param_dict.get(
        "purl",
        "pkg:oci/test-component@sha256%3Asomesha?repository_url=somewhere.com/something/test-component&tag=latest",
    )
    param_dict["type"] = param_dict.get("type", "file")
    param_dict["qualifiers"] = param_dict.get("repository_url", "somewhere.com")

    return Component(**param_dict)


@pytest.mark.parametrize(
    argnames=["completed_process_fixture", "oci_component"],
    argvalues=[
        pytest.param(
            {"returncode": 0, "stdout": bytes("sha256:somesha", "utf-8")},
            {
                "purl": "pkg:oci/test-component@sha256%3Asomesha?repository_url=somewhere.com/something/test-component&tag=latest"  # pylint: disable=line-too-long
            },
        ),
    ],
    indirect=["completed_process_fixture", "oci_component"],
)
def test_collect_oci_success(
    plugin_fixture: CollectDockerPlugin,
    oci_component: Component,
    completed_process_fixture: CompletedProcess,
    monkeypatch: MonkeyPatch,
):
    """
    Test a successful run of the Docker Collector with an OCI package
    """
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=_mock_get_repos)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=lambda *_, **__: completed_process_fixture)

    collect_result = plugin_fixture.process_component(oci_component)

    assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"


@pytest.mark.parametrize(
    argnames=["completed_process_fixture", "oci_component"],
    argvalues=[
        pytest.param(
            {"returncode": 0, "stdout": bytes("sha256:differentsha", "utf-8")},
            {
                "purl": "pkg:oci/test-component@sha256%3Asomesha?repository_url=somewhere.com/something/test-component&tag=latest"  # pylint: disable=line-too-long
            },
        ),
    ],
    indirect=["completed_process_fixture", "oci_component"],
)
def test_collect_oci_fail_tag_mismatch(
    plugin_fixture: CollectDockerPlugin,
    oci_component: Component,
    completed_process_fixture: CompletedProcess,
    monkeypatch: MonkeyPatch,
):
    """
    Test a failure run of the Docker Collector with an OCI package <tag mismatch>
    """
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=_mock_get_repos)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=lambda *_, **__: completed_process_fixture)

    collect_result = plugin_fixture.process_component(oci_component)

    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message.startswith("Provided tag 'latest' image digest does not match 'sha256:somesha'")


@pytest.mark.parametrize(
    argnames=["completed_process_fixture", "oci_component"],
    argvalues=[
        pytest.param(
            {"returncode": 1, "stdout": bytes("sha256:somesha", "utf-8")},
            {
                "purl": "pkg:oci/test-component@sha256%3Asomesha?repository_url=somewhere.com/something/test-component&tag=latest"  # pylint: disable=line-too-long
            },
        ),
    ],
    indirect=["completed_process_fixture", "oci_component"],
)
def test_collect_oci_fail_no_image_digest(
    plugin_fixture: CollectDockerPlugin,
    oci_component: Component,
    completed_process_fixture: CompletedProcess,
    monkeypatch: MonkeyPatch,
):
    """
    Test a successful run of the Docker Collector with an OCI package
    """
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=_mock_get_repos)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=lambda *_, **__: completed_process_fixture)

    collect_result = plugin_fixture.process_component(oci_component)

    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message.startswith("Failure after 3 attempts, final message Failed to get image digest for")
