"""
Verify contents of a bundle
"""
import sys
import tarfile

from fnmatch import fnmatch
from pathlib import Path

test_dir = Path(sys.argv[1])

expected_contents = list(filter(None, (test_dir / "expected-tar-toc").read_text(encoding="utf-8").split("\n")))

with tarfile.open(name=str(test_dir / "bundle.tar.gz")) as tar:
    actual_contents = tar.getnames()

try:
    if len(expected_contents) != len(actual_contents):
        raise ValueError("Number of expected items doesn't match actual number of items")

    if failed := [
        expected
        for expected in expected_contents
        if not any(fnmatch(name=actual, pat=expected) for actual in actual_contents)
    ]:
        raise ValueError("\n".join(("Expected items not found:", *(f"  * {item}" for item in failed))))
except ValueError as ex:
    print(ex)
    sys.exit(-1)
