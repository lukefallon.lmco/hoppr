"""Models to perform SBOM validation checks."""

from __future__ import annotations

import json

from datetime import date, timedelta
from pathlib import Path
from typing import TYPE_CHECKING, Any, Final, TypeAlias

import hoppr_cyclonedx_models.cyclonedx_1_5 as cdx

from pydantic import ValidationError, root_validator, validator

from hoppr.exceptions import HopprValidationError
from hoppr.models.licenses import LicenseExpressionItem, LicenseMultipleItem
from hoppr.models.validation.base import BaseValidator
from hoppr.models.validation.checks import ValidateConfig
from hoppr.models.validation.code_climate import Issue, IssueList, IssueSeverity
from hoppr.models.validation.json_mapper import JSONLocationMapper


if TYPE_CHECKING:
    DictStrAny: TypeAlias = dict[str, Any]


# Get previously parsed ValidateConfig. Each call returns
# the existing config without instantiating a new one.
_CONFIG: Final[ValidateConfig] = ValidateConfig()


def _field_validator(*field_names: str, check_name: str) -> classmethod[Any, Any, Any]:
    """Wrap a callable with the pydantic `root_validator` decorator."""

    def _validator(_cls: type[BaseValidator], values: DictStrAny) -> DictStrAny:
        field_values = [values.get(name) for name in field_names]

        if not any(field_values):
            missing_fields = ", ".join(f"'{name}'" for name in field_names)
            msg = f"Missing field{'s: one of' if len(field_names) > 1 else ':'} {missing_fields}"

            raise HopprValidationError(msg, check_name=check_name)

        return values

    # Assign validator method a unique name
    _validator.__name__ = f"_validate_{'_'.join(field_names)}"

    return root_validator(allow_reuse=True, pre=True)(_validator)


def _get_file_issue_list(exc: ValidationError, sbom_file: Path) -> IssueList:
    issue_list = IssueList()
    location_mapper = JSONLocationMapper.load_file(sbom_file)

    for error in exc.errors():
        ctx = error.get("ctx", {})
        check_name = ctx.get("check_name", "")

        if not (locations := location_mapper.find(_get_file_location(error["loc"], sbom_file))):
            continue  # pragma: no cover

        location, *_ = locations

        issue_list.append(
            Issue(
                check_name=check_name,
                description=error.get("msg", ""),
                location=location,
                severity=_get_severity(error["loc"], check_name),
            )
        )

    return issue_list


def _get_file_location(error_loc: tuple[int | str, ...], sbom_file: Path) -> DictStrAny:
    file_loc = json.loads(sbom_file.read_bytes().decode(encoding="utf-8"))

    for sub_loc in error_loc:
        match sub_loc:
            case str():
                file_loc = file_loc.get(sub_loc, file_loc)

                if isinstance(file_loc, str):
                    return {sub_loc: file_loc}

            case int():
                file_loc = file_loc[sub_loc]

    return file_loc


def _get_severity(location: tuple[int | str, ...], check_name: str) -> IssueSeverity:
    match location:
        case ("components", int(), "licenses", int(), *_):
            check_config = _CONFIG.component_checks.licenses.dict(by_alias=True).get(check_name, "ignore")

        case ("components", int(), *_):
            check_config = _CONFIG.component_checks.dict(by_alias=True).get(check_name, "ignore")

        case ("metadata", "licenses", int(), *_):
            check_config = _CONFIG.metadata_checks.licenses.dict(by_alias=True).get(
                check_name, "ignore"
            )  # pragma: no cover

        case ("metadata", *_):
            check_config = _CONFIG.metadata_checks.dict(by_alias=True).get(check_name, "ignore")

        case (*_,):
            check_config = _CONFIG.sbom_checks.dict(by_alias=True).get(check_name, "ignore")

    match check_config:
        case "ignore":
            severity = IssueSeverity.INFO

        case "warn":  # pragma: no cover
            severity = IssueSeverity.MINOR

        case "error":  # pragma: no cover
            severity = IssueSeverity.MAJOR

    return severity


class LicenseValidator(BaseValidator):
    """Model to perform validation checks on licenses."""

    class Config(BaseValidator.Config):
        """Config for LicenseValidator model."""

    __root__: LicenseMultipleItem | LicenseExpressionItem

    @root_validator
    @classmethod
    def _validate_license_types(cls, values: DictStrAny) -> DictStrAny:
        license_ = values["__root__"]

        if (
            isinstance(license_, LicenseMultipleItem)
            and license_.license.licensing
            and license_.license.licensing.licenseTypes
        ):
            return values

        raise HopprValidationError("Missing or invalid licenseTypes", check_name="licenses.license-types")

    @root_validator
    @classmethod
    def _validate_license_name_or_id(cls, values: DictStrAny) -> DictStrAny:
        license_ = values["__root__"]

        if isinstance(license_, LicenseMultipleItem) and (license_.license.name or license_.license.id):
            return values

        raise HopprValidationError("Missing license name or id", check_name="licenses.name-or-id")  # pragma: no cover

    @root_validator
    @classmethod
    def _validate_license_last_renewal(cls, values: DictStrAny) -> DictStrAny:
        license_ = values["__root__"]

        if (
            isinstance(license_, LicenseMultipleItem)
            and license_.license.licensing
            and license_.license.licensing.lastRenewal
        ):
            return values

        raise HopprValidationError("Missing or invalid lastRenewal", check_name="licenses.last-renewal")

    @root_validator
    @classmethod
    def _validate_license_purchase_order(cls, values: DictStrAny) -> DictStrAny:
        license_ = values["__root__"]

        if (
            isinstance(license_, LicenseMultipleItem)
            and license_.license.licensing
            and license_.license.licensing.purchaseOrder
        ):
            return values

        raise HopprValidationError("Missing or invalid purchaseOrder", check_name="licenses.purchase-order")

    @root_validator
    @classmethod
    def _validate_license_expiration(cls, values: DictStrAny) -> DictStrAny:
        license_ = values["__root__"]

        if (
            isinstance(license_, LicenseMultipleItem)
            and license_.license.licensing
            and license_.license.licensing.expiration
            and license_.license.licensing.expiration.date() < date.today() + timedelta(days=30)
        ):
            raise HopprValidationError(
                "License expired or expiring within %d days", check_name="licenses.expiration"
            )  # pragma: no cover

        return values


LicenseValidator.update_forward_refs()


class ComponentValidator(BaseValidator, cdx.Component):
    """Model to perform validation checks on components."""

    class Config(BaseValidator.Config):
        """Config for ComponentValidator model."""

    licenses: list[LicenseValidator] | None = None
    components: list[ComponentValidator] | None = None  # type: ignore[assignment]

    _validate_supplier: classmethod = _field_validator("supplier", check_name="components.supplier-field")
    _validate_name: classmethod = _field_validator("name", check_name="components.name-field")
    _validate_version: classmethod = _field_validator("version", check_name="components.version-field")
    _validate_unique_id: classmethod = _field_validator("cpe", "purl", "swid", check_name="components.unique-id")

    @validator("licenses", always=True, pre=True)
    @classmethod
    def _validate_licenses_field(cls, licenses: list[DictStrAny] | None) -> list[DictStrAny]:
        if not licenses:
            raise HopprValidationError("Missing field: 'licenses'", check_name="components.licenses-field")

        return licenses


ComponentValidator.update_forward_refs()


class MetadataValidator(BaseValidator, cdx.Metadata):
    """Model to perform validation checks on SBOM metadata."""

    class Config(BaseValidator.Config):
        """Config for MetadataValidator model."""

    timestamp: str | None = None  # type: ignore[assignment]
    tools: list | dict | None = None  # type: ignore[assignment]
    component: ComponentValidator | None = None
    licenses: list[LicenseValidator] | None = None

    _validate_timestamp: classmethod = _field_validator("timestamp", check_name="metadata.timestamp")
    _validate_authors: classmethod = _field_validator("authors", "tools", check_name="metadata.authors")
    _validate_licenses_field: classmethod = _field_validator("licenses", check_name="metadata.licenses-field")
    _validate_supplier: classmethod = _field_validator("supplier", check_name="metadata.supplier-field")


MetadataValidator.update_forward_refs()


class SbomValidator(BaseValidator, cdx.CyclonedxSoftwareBillOfMaterialsStandard):
    """Model to perform validation checks on overall SBOM document."""

    class Config(BaseValidator.Config):
        """Config for SbomValidator model."""

    metadata: MetadataValidator | None = None
    components: list[ComponentValidator] | None = None  # type: ignore[assignment]

    _validate_unique_id: classmethod = _field_validator("serialNumber", check_name="sbom.unique-id")

    @root_validator(pre=True)
    @classmethod
    def _remove_schema_field(cls, values: DictStrAny) -> DictStrAny:
        values.pop("$schema", None)

        return values

    @validator("components", always=True, pre=True)
    @classmethod
    def _validate_components_field(cls, components: list[DictStrAny] | None) -> list[DictStrAny]:
        if not components:
            raise HopprValidationError("Missing field: 'components'", check_name="sbom.components-field")

        return components

    @validator("vulnerabilities", always=True, pre=True)
    @classmethod
    def _validate_vulnerabilities_field(cls, vulnerabilities: list[DictStrAny] | None) -> list[DictStrAny]:
        if not vulnerabilities:
            raise HopprValidationError("Missing field: 'vulnerabilities'", check_name="sbom.vulnerabilities-field")

        return vulnerabilities

    @validator("specVersion", always=True, pre=True)
    @classmethod
    def _validate_spec_version(cls, spec_version: str) -> str:
        match spec_version:
            case "1.2":
                raise HopprValidationError(
                    f"Got specVersion {spec_version}; cannot be validated",
                    check_name="sbom.spec-version",
                )
            case "1.3" | "1.4":
                raise HopprValidationError(
                    f"Got specVersion {spec_version}; should be 1.5",
                    check_name="sbom.spec-version",
                )
            case "1.5":
                return spec_version
            case _:
                raise HopprValidationError(
                    f"Got an invalid specVersion of {spec_version}; should be 1.5",
                    check_name="sbom.spec-version",
                )


SbomValidator.update_forward_refs()


def validate(sbom_file: Path) -> IssueList:
    """Run validation against SBOM(s) with specified configuration.

    Args:
        sbom_files: List of files to run validation against.
    """
    issue_list = IssueList()

    try:
        SbomValidator.parse_file(sbom_file)
    except ValidationError as exc:
        # Capture errors and create Code Climate issue for each
        issue_list.extend(_get_file_issue_list(exc, sbom_file))

    return issue_list
