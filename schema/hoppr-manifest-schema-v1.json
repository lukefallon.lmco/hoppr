{
  "title": "ManifestFile",
  "description": "Data model to describe a single manifest file",
  "type": "object",
  "properties": {
    "kind": {
      "title": "Kind",
      "enum": [
        "Manifest"
      ],
      "type": "string"
    },
    "metadata": {
      "title": "Metadata",
      "description": "Metadata for the file",
      "allOf": [
        {
          "$ref": "#/definitions/HopprMetadata"
        }
      ]
    },
    "schemaVersion": {
      "title": "Schema Version",
      "default": "v1",
      "type": "string"
    },
    "sboms": {
      "title": "Sboms",
      "default": [],
      "type": "array",
      "items": {
        "anyOf": [
          {
            "$ref": "#/definitions/LocalFile"
          },
          {
            "$ref": "#/definitions/OciFile"
          },
          {
            "$ref": "#/definitions/UrlFile"
          }
        ]
      }
    },
    "repositories": {
      "title": "Repositories",
      "description": "Maps supported PURL types to package repositories/registries",
      "allOf": [
        {
          "$ref": "#/definitions/Repositories"
        }
      ]
    },
    "includes": {
      "title": "Includes",
      "description": "List of manifest files to load",
      "default": [],
      "type": "array",
      "items": {
        "anyOf": [
          {
            "$ref": "#/definitions/LocalFile"
          },
          {
            "$ref": "#/definitions/UrlFile"
          }
        ]
      }
    }
  },
  "required": [
    "kind",
    "repositories"
  ],
  "additionalProperties": false,
  "definitions": {
    "HopprMetadata": {
      "title": "HopprMetadata",
      "description": "Metadata data model",
      "type": "object",
      "properties": {
        "name": {
          "title": "Name",
          "type": "string"
        },
        "version": {
          "title": "Version",
          "anyOf": [
            {
              "type": "string"
            },
            {
              "type": "integer"
            }
          ]
        },
        "description": {
          "title": "Description",
          "type": "string"
        }
      },
      "required": [
        "name",
        "version",
        "description"
      ]
    },
    "LocalFile": {
      "title": "LocalFile",
      "description": "LocalFile data model",
      "type": "object",
      "properties": {
        "local": {
          "title": "Local",
          "type": "string",
          "format": "path"
        }
      },
      "required": [
        "local"
      ],
      "additionalProperties": false
    },
    "OciFile": {
      "title": "OciFile",
      "description": "OciFile data model",
      "type": "object",
      "properties": {
        "oci": {
          "title": "Oci",
          "anyOf": [
            {
              "type": "string",
              "minLength": 1,
              "maxLength": 65536,
              "format": "uri"
            },
            {
              "type": "string"
            }
          ]
        }
      },
      "required": [
        "oci"
      ],
      "additionalProperties": false
    },
    "UrlFile": {
      "title": "UrlFile",
      "description": "UrlFile data model",
      "type": "object",
      "properties": {
        "url": {
          "title": "Url",
          "anyOf": [
            {
              "type": "string",
              "minLength": 1,
              "maxLength": 2083,
              "format": "uri"
            },
            {
              "type": "string",
              "minLength": 1,
              "maxLength": 65536,
              "format": "uri"
            },
            {
              "type": "string",
              "minLength": 1,
              "maxLength": 65536,
              "format": "uri"
            },
            {
              "type": "string"
            }
          ]
        }
      },
      "required": [
        "url"
      ],
      "additionalProperties": false
    },
    "Repository": {
      "title": "Repository",
      "description": "Repository data model",
      "type": "object",
      "properties": {
        "url": {
          "title": "Url",
          "pattern": "^([^:/?#]+:(?=//))?(//)?(([^:]+(?::[^@]+?)?@)?[^@/?#:]*(?::\\d+?)?)?[^?#]*(\\?[^#]*)?(#.*)?",
          "type": "string"
        },
        "description": {
          "title": "Description",
          "type": "string"
        }
      },
      "required": [
        "url"
      ],
      "additionalProperties": false
    },
    "Repositories": {
      "title": "Repositories",
      "description": "Repositories data model",
      "type": "object",
      "properties": {
        "cargo": {
          "title": "Cargo",
          "default": [],
          "uniqueItems": true,
          "type": "array",
          "items": {
            "$ref": "#/definitions/Repository"
          }
        },
        "deb": {
          "title": "Deb",
          "default": [],
          "uniqueItems": true,
          "type": "array",
          "items": {
            "$ref": "#/definitions/Repository"
          }
        },
        "docker": {
          "title": "Docker",
          "default": [],
          "uniqueItems": true,
          "type": "array",
          "items": {
            "$ref": "#/definitions/Repository"
          }
        },
        "gem": {
          "title": "Gem",
          "default": [],
          "uniqueItems": true,
          "type": "array",
          "items": {
            "$ref": "#/definitions/Repository"
          }
        },
        "generic": {
          "title": "Generic",
          "default": [],
          "uniqueItems": true,
          "type": "array",
          "items": {
            "$ref": "#/definitions/Repository"
          }
        },
        "git": {
          "title": "Git",
          "default": [],
          "uniqueItems": true,
          "type": "array",
          "items": {
            "$ref": "#/definitions/Repository"
          }
        },
        "github": {
          "title": "Github",
          "default": [],
          "uniqueItems": true,
          "type": "array",
          "items": {
            "$ref": "#/definitions/Repository"
          }
        },
        "gitlab": {
          "title": "Gitlab",
          "default": [],
          "uniqueItems": true,
          "type": "array",
          "items": {
            "$ref": "#/definitions/Repository"
          }
        },
        "golang": {
          "title": "Golang",
          "default": [],
          "uniqueItems": true,
          "type": "array",
          "items": {
            "$ref": "#/definitions/Repository"
          }
        },
        "helm": {
          "title": "Helm",
          "default": [],
          "uniqueItems": true,
          "type": "array",
          "items": {
            "$ref": "#/definitions/Repository"
          }
        },
        "maven": {
          "title": "Maven",
          "default": [],
          "uniqueItems": true,
          "type": "array",
          "items": {
            "$ref": "#/definitions/Repository"
          }
        },
        "npm": {
          "title": "Npm",
          "default": [],
          "uniqueItems": true,
          "type": "array",
          "items": {
            "$ref": "#/definitions/Repository"
          }
        },
        "nuget": {
          "title": "Nuget",
          "default": [],
          "uniqueItems": true,
          "type": "array",
          "items": {
            "$ref": "#/definitions/Repository"
          }
        },
        "oci": {
          "title": "Oci",
          "default": [],
          "uniqueItems": true,
          "type": "array",
          "items": {
            "$ref": "#/definitions/Repository"
          }
        },
        "pypi": {
          "title": "Pypi",
          "default": [],
          "uniqueItems": true,
          "type": "array",
          "items": {
            "$ref": "#/definitions/Repository"
          }
        },
        "raw": {
          "title": "Raw",
          "default": [],
          "uniqueItems": true,
          "type": "array",
          "items": {
            "$ref": "#/definitions/Repository"
          }
        },
        "repo": {
          "title": "Repo",
          "default": [],
          "uniqueItems": true,
          "type": "array",
          "items": {
            "$ref": "#/definitions/Repository"
          }
        },
        "rpm": {
          "title": "Rpm",
          "default": [],
          "uniqueItems": true,
          "type": "array",
          "items": {
            "$ref": "#/definitions/Repository"
          }
        }
      },
      "additionalProperties": false
    }
  }
}