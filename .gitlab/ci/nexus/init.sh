#!/usr/bin/env sh

readonly NEXUS_API_URL="http://localhost:8081/service/rest/v1"
readonly PW_FILE="${CI_PROJECT_DIR}/nexus-data/admin.password"

[ ! -e "${PW_FILE}" ] && {
  echo "Nexus admin initial password file not found; admin password already set."
  exit
}

#-----------------------------------------------------------
# Send Nexus API request from inside the service container
#-----------------------------------------------------------
curl_nexus() {
  docker compose exec nexus \
    curl --fail --location \
    --output /dev/null --silent \
    --header "Accept: application/json" "$@"
}

initial_password="$(cat "${PW_FILE}")"

echo "Updating Nexus admin password..."
curl_nexus --request PUT \
  --header "Content-Type: text/plain" \
  --data "${NEXUS_PW}" --user "admin:${initial_password}" \
  --url "${NEXUS_API_URL}/security/users/admin/change-password"

echo "Enabling anonymous access..."
curl_nexus --request PUT \
  --header "Content-Type: application/json" \
  --data '{"enabled":true, "userId":"anonymous", "realmName":"NexusAuthorizingRealm"}' \
  --user "admin:${NEXUS_PW}" --url "${NEXUS_API_URL}/security/anonymous"

echo "Nexus startup complete."
