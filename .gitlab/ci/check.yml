---
.base-python:
  image: $CI_REGISTRY_IMAGE/ci:$CI_COMMIT_REF_SLUG
  needs:
    - job: poetry
  variables:
    POETRY_CACHE_DIR: $CI_PROJECT_DIR/.cache/pypoetry
    POETRY_VIRTUALENVS_CREATE: "true"
    POETRY_VIRTUALENVS_IN_PROJECT: "true"
    POETRY_VIRTUALENVS_PATH: $CI_PROJECT_DIR/.venv
  cache:
    key: poetry-$CI_COMMIT_SHORT_SHA
    policy: pull
    paths:
      - .cache
      - .venv
      - poetry.lock

poetry:
  stage: build
  extends: .base-python
  needs:
    - job: build-image:ci
      optional: true
    - job: semantic-release:dry-run
  script:
    # Install project dependencies with poetry
    - poetry lock
    - poetry install
  cache:
    policy: push

generate-default-validate-config:
  extends: .base-python
  stage: build
  script:
    - poetry run hopctl generate validate-config
  artifacts:
    paths:
      - hoppr/models/validation/profiles/default.config.yml

build-dist:
  extends: .base-python
  stage: build
  needs:
    - job: poetry
    - job: semantic-release:dry-run
  script:
    # Remove "v" prefix from release version if present
    - RELEASE_VERSION="${RELEASE_VERSION#v}"

    # Update `__version__` variable and pyproject.toml with new version, then build wheel
    - sed_script="$(printf 's/^__version__ = ".*"/__version__ = "%s"/' "$RELEASE_VERSION")"
    - sed --in-place "$sed_script" hoppr/__init__.py
    - poetry version $RELEASE_VERSION
    - poetry build
  artifacts:
    paths:
      - dist/*

unit-tests:
  extends: .base-python
  stage: check
  script:
    - poetry run pytest
  coverage: /(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/
  artifacts:
    when: always
    paths:
      - coverage.xml
    reports:
      junit:
        - test.xml
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml

check-format:
  extends: .base-python
  stage: check
  script:
    - poetry run black --check hoppr test/unit

check-sourcery:
  extends: .base-python
  stage: check
  variables:
    FORCE_COLOR: "1"
  before_script:
    - poetry run sourcery login --token $SOURCERY_TOKEN
  script:
    - poetry run --ansi sourcery review --check --verbose hoppr test/unit

check-lint:
  extends: .base-python
  stage: check
  script:
    - poetry run pylint hoppr test/unit

check-types:
  extends: .base-python
  stage: check
  script:
    - poetry run mypy
  artifacts:
    when: always
    paths:
      - cobertura.xml
    reports:
      junit:
        - mypy.xml
      coverage_report:
        coverage_format: cobertura
        path: cobertura.xml

validate-renovate-config:
  image: docker.io/renovate/renovate:34.124
  needs: []
  stage: check
  variables:
    RENOVATE_CONFIG_FILE: renovate.json
  script:
    - renovate-config-validator

.shell-lint:
  stage: check
  needs: []
  before_script:
    - apk update && apk add git
    - scripts="$(git ls-files --exclude='*.sh' --ignored --cached)"

shellcheck:
  extends: .shell-lint
  image: docker.io/koalaman/shellcheck-alpine:stable
  script:
    - shellcheck --version
    - shellcheck $scripts

shfmt:
  extends: .shell-lint
  image: docker.io/mvdan/shfmt:v3-alpine
  script:
    - shfmt --version
    - shfmt --binary-next-line --keep-padding --case-indent --space-redirects --indent=2 --diff $scripts
