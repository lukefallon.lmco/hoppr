#!/usr/bin/env bash

readonly PYTHON_VERSION="3.10.8"
readonly PYTHON_DOWNLOAD_URL="https://www.python.org/ftp/python/${PYTHON_VERSION}/Python-${PYTHON_VERSION}.tgz"

# Install python build dependencies
dnf install --assumeyes dnf-plugins-core
dnf config-manager --enable crb
dnf builddep --assumeyes python3

curl --silent --location --fail --show-error --url "${PYTHON_DOWNLOAD_URL}" | tar --extract --gzip
pushd "Python-${PYTHON_VERSION}" || exit

# Compile and install python source code
./configure --quiet --enable-optimizations --with-lto --with-ensurepip=upgrade
make --quiet --jobs
make --quiet altinstall
popd || exit

rm -r "Python-${PYTHON_VERSION}"

pushd /usr/local/bin || exit

# Create symbolic links
for bin in pip python; do
  ln --symbolic --force "${bin}3.10" "/usr/local/bin/${bin}3"
  ln --symbolic --force "${bin}3" "/usr/local/bin/${bin}"
done

popd || exit

# Remove python build dependencies
dnf history undo --assumeyes last
dnf clean all
