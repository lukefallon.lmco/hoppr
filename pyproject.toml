[tool.poetry]
name = "hoppr"
version = "1.12.0-dev.16"
description = "A tool for defining, verifying, and transferring software dependencies between environments."
authors = ["LMCO Open Source <open.source@lmco.com>"]
license = "MIT"
readme = "README.md"
homepage = "https://hoppr.dev"
repository = "https://gitlab.com/hoppr/hoppr"

keywords = [
  "packaging",
  "reports",
  "build dependencies",
  "software bill of materials"
]

classifiers = [
  "Development Status :: 3 - Alpha",
  "Intended Audience :: Information Technology",
  "Topic :: Communications :: File Sharing",
  "Topic :: Software Development :: Version Control",
  "Topic :: System :: Software Distribution"
]

[[tool.poetry.packages]]
include = "hoppr"

[tool.poetry.scripts]
hopctl = "hoppr.cli:app"

[tool.poetry.dependencies]
python = "^3.10"
hoppr-cyclonedx-models = ">=0.5.5, <1"
in-toto = "^1.4.0"
jc = "^1.25.2"
Jinja2 = "^3.1.3"
jmespath = "^1.0.1"
oras = ">=0.1.28, <1"
packageurl-python = ">=0.15.0, <1"
psutil = "^5.9.8"
pydantic-yaml = ">=0.11.2, <1"
PyYAML = "^6.0.1"
rapidfuzz = "^3.7.0"
requests = "^2.31.0"
ruamel-yaml = ">=0.18.6, <1"
securesystemslib = ">=0.31.0, <1"
typing-extensions = "^4.10.0"

[tool.poetry.dependencies.pydantic]
extras = ["email"]
version = "^1.10.15"

[tool.poetry.dependencies.typer-slim]
extras = ["standard"]
version = ">=0.12.0, <1"

[tool.poetry.group.dev.dependencies]
black = "^23.10.0"
dill = "0.*"
isort = "^5.13.2"
pre-commit = "^3.7.0"
pylint = "^3.1.0"
setuptools = "^69.2.0"
shfmt-py = "^3.7.0.1"
sourcery = "^1.16.0"

[tool.poetry.group.test.dependencies]
pytest = "^8.1.1"
pytest-cov = "^5.0.0"
pytest-pretty = "^1.2.0"

[tool.poetry.group.typing.dependencies]
types-colorama = ">=0.4.15.12, <1"
types-docutils = ">=0.20.0.3, <1"
types-jmespath = "^1.0.2.20240106"
types-jsonschema = "^4.21.0.20240331"
types-psutil = "^5.9.5.20240316"
types-pygments = "^2.17.0.20240310"
types-PyYAML = "^6.0.12.20240311"
types-requests = "^2.31.0.20240403"
types-setuptools = "^69.2.0.20240317"
types-xmltodict = ">=0.13.0.3, <1"

[tool.poetry.group.typing.dependencies.mypy]
extras = ["reports"]
version = "^1.6.1"

[tool.poetry.plugins."hoppr.plugin"]
rpm = "hoppr.plugins.collect.rpm:CollectRpmPlugin"

# Legacy Hoppr plugin entry points
bundle_tar = "hoppr.core_plugins.bundle_tar:TarBundlePlugin"
collect_apt_plugin = "hoppr.core_plugins.collect_apt_plugin:CollectAptPlugin"
collect_cargo_plugin = "hoppr.core_plugins.collect_cargo_plugin:CollectCargoPlugin"
collect_dnf_plugin = "hoppr.core_plugins.collect_dnf_plugin:CollectDnfPlugin"
collect_docker_plugin = "hoppr.core_plugins.collect_docker_plugin:CollectDockerPlugin"
collect_gem_plugin = "hoppr.core_plugins.collect_gem_plugin:CollectGemPlugin"
collect_git_plugin = "hoppr.core_plugins.collect_git_plugin:CollectGitPlugin"
collect_golang_plugin = "hoppr.core_plugins.collect_golang_plugin:CollectGolangPlugin"
collect_helm_plugin = "hoppr.core_plugins.collect_helm_plugin:CollectHelmPlugin"
collect_maven_plugin = "hoppr.core_plugins.collect_maven_plugin:CollectMavenPlugin"
collect_nexus_search = "hoppr.core_plugins.collect_nexus_search:CollectNexusSearch"
collect_npm_plugin = "hoppr.core_plugins.collect_npm_plugin:CollectNpmPlugin"
collect_nuget_plugin = "hoppr.core_plugins.collect_nuget_plugin:CollectNugetPlugin"
collect_pypi_plugin = "hoppr.core_plugins.collect_pypi_plugin:CollectPypiPlugin"
collect_raw_plugin = "hoppr.core_plugins.collect_raw_plugin:CollectRawPlugin"
collect_yum_plugin = "hoppr.core_plugins.collect_yum_plugin:CollectYumPlugin"
composite_collector = "hoppr.core_plugins.composite_collector:CompositeCollector"
delta_sbom = "hoppr.core_plugins.delta_sbom:DeltaSbom"
oras_bundle = "hoppr.core_plugins.oras_bundle:OrasBundlePlugin"
report_generator = "hoppr.core_plugins.report_generator:ReportGenerator"

[tool.black]
color = true
line-length = 120
target-version = ["py310"]

[tool.coverage.report]
exclude_also = [
  "if __name__ == .__main__.:",
  "if not TYPE_CHECKING:",
  "if self.debug:",
  "if settings.DEBUG:",
  "if TYPE_CHECKING:",
  "pragma: no cover",
  "raise NotImplementedError"
]
precision = 2
show_missing = true
skip_covered = true
skip_empty = true

[tool.isort]
atomic = true
known_third_party = ["in_toto"]
line_length = 120
lines_after_imports = 2
lines_between_types = 1
profile = "black"
py_version = 310
skip_gitignore = true
src_paths = ["hoppr", "test/unit"]

[tool.mypy]
cobertura_xml_report = "."
enable_error_code = ["ignore-without-code"]
explicit_package_bases = true
follow_imports = "silent"
incremental = false
junit_xml = "mypy.xml"
mypy_path = "stubs"
namespace_packages = true
no_implicit_reexport = true
packages = ["hoppr", "test.unit"]
plugins = ["pydantic.mypy"]
pretty = true
python_version = "3.10"
show_error_codes = true
warn_redundant_casts = true
warn_unused_configs = true
warn_unused_ignores = true

[[tool.mypy.overrides]]
module = [
  "in_toto.*",
  "jmespath",
  "oras.*",
  "packageurl",
  "rapidfuzz",
  "securesystemslib.*",
]
allow_untyped_defs = true
allow_incomplete_defs = true
allow_untyped_calls = true
ignore_missing_imports = true
implicit_reexport = true

[tool.pydantic-mypy]
init_forbid_extra = true
init_typed = true
warn_required_dynamic_aliases = true
warn_untyped_fields = true

[tool.pylint.main]
extension-pkg-whitelist = ["pydantic"]
init-hook = """\
  from pylint.config import find_default_config_files;\
  import sys;\
  sys.path.append(str(list(find_default_config_files())[0].parent / 'hoppr'))\
  """
jobs = 0
py-version = "3.10"

[tool.pylint.basic]
include-naming-hint = true

[tool.pylint.classes]
valid-metaclass-classmethod-first-arg = ["cls", "mcs"]

[tool.pylint.format]
max-line-length = 120

[tool.pyright]
include = ["hoppr", "test/unit"]
pythonPlatform = "All"
pythonVersion = "3.10"
reportArgumentType = false
reportCallIssue = false
reportGeneralTypeIssues = false

[tool.pytest.ini_options]
addopts = [
  "--strict-markers",
  "--cov=hoppr",
  "--cov-report=term-missing",
  "--cov-report=xml:coverage.xml",
  "--cov-report=lcov:lcov.info",
  "--cov-fail-under=100",
  "--import-mode=importlib",
  "--junit-xml=test.xml"
]
console_output_style = "progress"
junit_family = "xunit2"
pythonpath = ["."]
testpaths = ["test/unit"]

[build-system]
requires = ["poetry-core"]
build-backend = "poetry.core.masonry.api"
